import {AboutPage} from './about.po'
import { browser, logging } from 'protractor';

describe('About page View',()=>{
    let page: AboutPage;

    beforeEach(() => {
        page = new AboutPage();
      });

    it('should display Title message', () => {
        page.navigateTo();
        expect(page.getTitleText()).toEqual('About Us');
    });

    it('should display footer', ()=>{
        page.navigateTo();
        expect(page.getFooter()).toBeTruthy();
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
          level: logging.Level.SEVERE,
        } as logging.Entry));
    });
    


})