import { browser, by, element } from 'protractor';

export class AboutPage{
    navigateTo(){
        return browser.get('/about') 
    }

    getTitleText(){
        return element(by.css('.title')).getText()
    }

    getFooter(){
        return element(by.css('.footer'))
    }
}

