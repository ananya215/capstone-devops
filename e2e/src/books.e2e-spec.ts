import {BooksPage} from './books.po';
import { browser, by, element } from 'protractor';
var books = require('../../src/app/books/books.json')


describe('BooksPage tests',()=>{
    let page:BooksPage;

    beforeEach(()=>{
        page = new BooksPage();
    })
    it('should have 10 books', () => {
        console.log(books.books.length)
        
        expect(books.books.length).toEqual(10);
    });
    it('should have Welcome Message and date time', () => {
        page.navigateTo();
       //expect(element(by.css('.welcome h1')).getText()).toEqual('Welcome,')
        // expect(element(by.binding('time'))).toEqual(new Date().toLocaleTimeString());
        // expect(element(by.binding('day'))).toEqual(new Date().toDateString());
    });
})