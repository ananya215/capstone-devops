FROM nginx:1.16.0-alpine as prod-stage
COPY /dist/checkinFED /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
