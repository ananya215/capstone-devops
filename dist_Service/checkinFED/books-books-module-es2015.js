(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["books-books-module"],{

/***/ "./src/app/books/addbook-guard-service.ts":
/*!************************************************!*\
  !*** ./src/app/books/addbook-guard-service.ts ***!
  \************************************************/
/*! exports provided: AddBookGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBookGuardService", function() { return AddBookGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class AddBookGuardService {
    canDeactivate(component) {
        if (component.addBookForm.dirty && !component.addBookForm.submitted) {
            return confirm('Are you sure you want to leave ?');
        }
        return true;
    }
}
AddBookGuardService.ɵfac = function AddBookGuardService_Factory(t) { return new (t || AddBookGuardService)(); };
AddBookGuardService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AddBookGuardService, factory: AddBookGuardService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddBookGuardService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], null, null); })();


/***/ }),

/***/ "./src/app/books/addbook.component.ts":
/*!********************************************!*\
  !*** ./src/app/books/addbook.component.ts ***!
  \********************************************/
/*! exports provided: AddBookComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBookComponent", function() { return AddBookComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./books.service */ "./src/app/books/books.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");






const _c0 = ["addForm"];
function AddBookComponent_div_20_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Title is required required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AddBookComponent_div_20_div_1_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.required);
} }
function AddBookComponent_div_40_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Author name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_40_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Author name is invalid");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_40_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AddBookComponent_div_40_div_1_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, AddBookComponent_div_40_div_2_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](33);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r5.errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r5.pattern.invalid);
} }
function AddBookComponent_div_57_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Price is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_57_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AddBookComponent_div_57_div_1_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r8.errors.required);
} }
function AddBookComponent_div_58_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Quantity is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_58_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Invalid quantity");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddBookComponent_div_58_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AddBookComponent_div_58_div_1_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, AddBookComponent_div_58_div_2_Template, 3, 0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.errors.invalid);
} }
class AddBookComponent {
    constructor(_booksService, router) {
        this._booksService = _booksService;
        this.router = router;
        // selectedFile:File  = null;
        // imagePath="../../assets/images/";
        // onFileSelected(event){
        //   this.selectedFile = <File>event.target.files[0];
        //   this.image=this.selectedFile.name;
        //   this.imagePath += this.image;
        // }
        this.image = "";
    }
    ngOnInit() {
    }
    addbook(formValue) {
        console.log(formValue);
        const newbook = {
            title: formValue.title,
            subtitle: formValue.subtitle,
            description: formValue.description,
            author: formValue.author,
            category: formValue.category,
            price: formValue.price,
            quantity: formValue.quantity,
        };
        this._booksService.addbook(newbook).subscribe((book) => {
            this.router.navigate(['/books']);
        });
    }
}
AddBookComponent.ɵfac = function AddBookComponent_Factory(t) { return new (t || AddBookComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
AddBookComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AddBookComponent, selectors: [["books-addbook"]], viewQuery: function AddBookComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.addBookForm = _t.first);
    } }, decls: 95, vars: 4, consts: [[1, "mainBg"], [1, "addbookform"], [1, "title"], [3, "ngSubmit"], ["addForm", "ngForm"], [1, "form-group"], ["for", "title"], [1, "required", 2, "color", "red"], ["type", "text", "name", "title", "ngModel", "", "placeholder", "title", "required", "", 1, "form-control", "form-control-sm", 2, "width", "40%"], ["titleRef", "ngModel"], [1, "form-group", 2, "margin-left", "45%", "margin-top", "-8.5%"], ["type", "text", "name", "subtitle", "ngModel", "", "placeholder", "subtitle", 1, "form-control", "form-control-sm", 2, "width", "100%"], ["subtitleRef", "ngModel"], [1, "row", "errors"], ["class", "col-sm-6", 4, "ngIf"], ["for", "exampleFormControlTextarea2"], ["id", "exampleFormControlTextarea2", "rows", "3", "name", "description", "ngModel", "", "placeholder", "description", 1, "form-control", "rounded-0"], ["descriptionRef", "ngModel"], ["for", "author"], ["type", "text", "pattern", "[a-zA-Z]{1,15}", "required", "", "name", "author", "ngModel", "", "placeholder", "author", 1, "form-control", "form-control-sm", 2, "width", "30%"], ["authorRef", "ngModel"], [1, "form-group", 2, "margin-left", "35%", "margin-top", "-8.5%"], ["for", "quantity"], ["type", "text", "name", "category", "ngModel", "", "placeholder", "category", 1, "form-control", "form-control-sm", 2, "width", "40%"], ["categoryRef", "ngModel"], [1, "row"], ["for", "price"], ["type", "number", "name", "price", "ngModel", "", "placeholder", "price", "required", "", 1, "form-control", "form-control-sm", 2, "width", "30%"], ["priceRef", "ngModel"], ["type", "number", "name", "quantity", "ngModel", "", "placeholder", "quantity", "required", "", 1, "form-control", "form-control-sm", 2, "width", "40%"], ["quantityRef", "ngModel"], ["class", "col-sm-4", 4, "ngIf"], ["type", "submit", 1, "btn", "btn-primary"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"], [1, "col-sm-6"], [4, "ngIf"], [2, "color", "red"], [1, "col-sm-4"]], template: function AddBookComponent_Template(rf, ctx) { if (rf & 1) {
        const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Enter the details ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AddBookComponent_Template_form_ngSubmit_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5); return ctx.addbook(_r0.value); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "*");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 8, 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Subtitle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 11, 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, AddBookComponent_div_20_Template, 2, 1, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Description");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "textarea", 16, 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Author");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "*");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 19, 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "label", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "input", 23, 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, AddBookComponent_div_40_Template, 3, 2, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "label", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "*");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "input", 27, 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Quantity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "*");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "input", 29, 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](57, AddBookComponent_div_57_Template, 2, 1, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, AddBookComponent_div_58_Template, 3, 2, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "i", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "BookZone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "i", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "a", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "i", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "a", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "i", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "a", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "i", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Mail us at:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, " Example.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, " Box 564, Disneyland");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, " Bangalore ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Visit us at");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, " BookZone Internet Private Limited,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, " Buildings Alyssa, Begonia &");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " Clove Embassy Tech Village,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](13);
        const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](33);
        const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](48);
        const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r5.invalid && (_r5.dirty || _r5.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r8.invalid && (_r8.dirty || _r8.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.invalid && (_r9.dirty || _r9.touched));
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NumberValueAccessor"]], styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 150vh;\n    overflow:hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:8%;\n    margin-left:17%;\n    position: absolute;\n    height: 100%;\n    width: 70%;\n    scroll-behavior: auto;\n    \n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n}\nform[_ngcontent-%COMP%]{\n    padding: 15px;\n}\n.alert-danger[_ngcontent-%COMP%]{\n  \n    border-radius: 5px;\n    font-size:14px;\n    width:40%;\n    \n\n}\n.errors[_ngcontent-%COMP%]{\n    font-size:12px;\n    font-style: italic\n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:65%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\naddress[_ngcontent-%COMP%]{\n       float: right;\n       font-size: 12px;\n       margin-top:-7%;\n       margin-right:30px;\n       text-align: left;\n   }\n.mailus[_ngcontent-%COMP%]{\n     font-size: 12px;\n     margin-top:-2%;\n    padding: 12px;\n     margin-left:63%;\n     text-align: left;\n   }\n.vl[_ngcontent-%COMP%] {\n     border-left: 1px solid black;\n     height: 83px;\n     margin-right:10%;\n     margin-left:-6%;\n     position: absolute;\n     margin-top:0px;\n   }\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYm9va3MvYWRkYm9vay5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksNENBQThEO0lBQzlELHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsMkJBQTJCO0lBQzNCLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsaUJBQWlCO0lBQ2pCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osVUFBVTtJQUNWLHFCQUFxQjs7QUFFekI7QUFDQTtJQUNJLGNBQWM7SUFDZCw2QkFBNkI7SUFDN0IsNkNBQTZDO0lBQzdDLHFCQUFxQjtJQUNyQixhQUFhO0lBQ2IsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQix5QkFBeUI7QUFDN0I7QUFDQTtJQUNJLGFBQWE7QUFDakI7QUFDQTs7SUFFSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLFNBQVM7OztBQUdiO0FBQ0E7SUFDSSxjQUFjO0lBQ2Q7QUFDSjtBQUVBOztJQUVJLGNBQWM7O0tBRWIsY0FBYztLQUNkLGlCQUFpQjtLQUNqQixhQUFhO0tBQ2IsU0FBUztLQUNULFVBQVU7SUFDWCxrQkFBa0I7S0FDakIsdUJBQXVCO1FBQ3BCLGtCQUFrQjtLQUNyQixrQkFBa0I7S0FDbEIsaUJBQWlCO0dBQ25CO0FBQ0E7T0FDSSxZQUFZO09BQ1osZUFBZTtPQUNmLGNBQWM7T0FDZCxpQkFBaUI7T0FDakIsZ0JBQWdCO0dBQ3BCO0FBQ0E7S0FDRSxlQUFlO0tBQ2YsY0FBYztJQUNmLGFBQWE7S0FDWixlQUFlO0tBQ2YsZ0JBQWdCO0dBQ2xCO0FBQ0E7S0FDRSw0QkFBNEI7S0FDNUIsWUFBWTtLQUNaLGdCQUFnQjtLQUNoQixlQUFlO0tBQ2Ysa0JBQWtCO0tBQ2xCLGNBQWM7R0FDaEI7QUFDQTtPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCO0FBQ0Y7S0FDSSxXQUFXO0tBQ1gsZUFBZTs7S0FFZixjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxnQkFBZ0I7Q0FDcEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLFdBQVc7S0FDWCxlQUFlO0tBQ2YsaUJBQWlCOztDQUVyQjtBQUNBO0tBQ0ksYUFBYTtLQUNiO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9ib29rcy9hZGRib29rLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbkJne1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL2Jvb2suanBnKSBuby1yZXBlYXQgY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtby1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tcy1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIG1pbi1oZWlnaHQ6IDE1MHZoO1xuICAgIG92ZXJmbG93OmhpZGRlbjtcbn1cbi5hZGRib29rZm9ybXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBtYXJnaW4tdG9wOjglO1xuICAgIG1hcmdpbi1sZWZ0OjE3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiA3MCU7XG4gICAgc2Nyb2xsLWJlaGF2aW9yOiBhdXRvO1xuICAgIFxufVxuLnRpdGxle1xuICAgIGZvbnQtc2l6ZTozMHB4O1xuICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrOyAqL1xuICAgIGJveC1zaGFkb3c6IDAgMTVweCAyNXB4IC0xMHB4IHJnYmEoMCwwLDAsLjM1KTtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNBMERFRTI7XG59XG5mb3Jte1xuICAgIHBhZGRpbmc6IDE1cHg7XG59XG4uYWxlcnQtZGFuZ2Vye1xuICBcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgZm9udC1zaXplOjE0cHg7XG4gICAgd2lkdGg6NDAlO1xuICAgIFxuXG59XG4uZXJyb3Jze1xuICAgIGZvbnQtc2l6ZToxMnB4O1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpY1xufVxuXG4uZm9vdGVyIHtcbiBcbiAgICBtYXJnaW4tbGVmdDo2JTtcbiAgICBcbiAgICAgbWFyZ2luLXRvcDo2NSU7XG4gICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICBib3R0b206IDA7XG4gICAgIHdpZHRoOiA4OSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGJvcmRlci1yYWRpdXM6NXB4O1xuICAgfVxuICAgYWRkcmVzc3tcbiAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgIG1hcmdpbi10b3A6LTclO1xuICAgICAgIG1hcmdpbi1yaWdodDozMHB4O1xuICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICB9XG4gICAubWFpbHVze1xuICAgICBmb250LXNpemU6IDEycHg7XG4gICAgIG1hcmdpbi10b3A6LTIlO1xuICAgIHBhZGRpbmc6IDEycHg7XG4gICAgIG1hcmdpbi1sZWZ0OjYzJTtcbiAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgIH1cbiAgIC52bCB7XG4gICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgYmxhY2s7XG4gICAgIGhlaWdodDogODNweDtcbiAgICAgbWFyZ2luLXJpZ2h0OjEwJTtcbiAgICAgbWFyZ2luLWxlZnQ6LTYlO1xuICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgIG1hcmdpbi10b3A6MHB4O1xuICAgfVxuICAgLmZvb3Rlci1pY29ue1xuICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgIG1hcmdpbi10b3A6MTVweDtcbiAgICAgICBtYXJnaW4tbGVmdDoxNXB4O1xuICAgfVxuIC5sb2dvLWZvb3RlcntcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIG1hcmdpbi10b3A6MjFweDtcbiAgICAgXG4gICAgIGZvbnQtc2l6ZToyMHB4O1xuIH1cbiAuZmItaWNvbntcbiAgICAgbWFyZ2luLWxlZnQ6LTEwJTtcbiB9XG4gI3NvY2lhbC1mYjpob3ZlciB7XG4gICAgIGNvbG9yOiAjM0I1OTk4O1xuIH1cbiAjc29jaWFsLXR3OmhvdmVyIHtcbiAgICAgY29sb3I6ICM0MDk5RkY7XG4gfVxuICNzb2NpYWwtZ3A6aG92ZXIge1xuICAgICBjb2xvcjogI2QzNDgzNjtcbiB9XG4gI3NvY2lhbC1lbTpob3ZlciB7XG4gICAgIGNvbG9yOiAjZjM5YzEyO1xuIH1cbiAuc29jaWFsLWljb25ze1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICBtYXJnaW4tbGVmdDoxNzBweDtcbiAgICAgXG4gfVxuIC5zb2NpYWwtaWNvbnMgYXtcbiAgICAgcGFkZGluZzogMTBweDtcbiAgICAgY29sb3I6cmdiKDkwLCAyMDEsIDIzOClcbiB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddBookComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'books-addbook',
                templateUrl: './addbook.component.html',
                styleUrls: ['./addbook.component.css']
            }]
    }], function () { return [{ type: _books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, { addBookForm: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['addForm', { static: false }]
        }] }); })();


/***/ }),

/***/ "./src/app/books/book-detail-guard.service.ts":
/*!****************************************************!*\
  !*** ./src/app/books/book-detail-guard.service.ts ***!
  \****************************************************/
/*! exports provided: BookDetailGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookDetailGuardService", function() { return BookDetailGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class BookDetailGuardService {
    constructor(_authService, _router) {
        this._authService = _authService;
        this._router = _router;
    }
    canActivate(route, state) {
        console.log(`Can ${state.url} be activated ?`);
        if (this._authService.isLoggedIn()) {
            console.log("Yes, the route can be activated as we are already logged in.");
            return true;
        }
        else {
            console.log("CANNOT ACTIVATE the route until logged in...");
            this._router.navigate(['signin']);
            return false;
        }
    }
}
BookDetailGuardService.ɵfac = function BookDetailGuardService_Factory(t) { return new (t || BookDetailGuardService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
BookDetailGuardService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: BookDetailGuardService, factory: BookDetailGuardService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BookDetailGuardService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/books/books-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/books/books-routing.module.ts ***!
  \***********************************************/
/*! exports provided: BooksRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksRoutingModule", function() { return BooksRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _books_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./books.component */ "./src/app/books/books.component.ts");
/* harmony import */ var _addbook_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addbook.component */ "./src/app/books/addbook.component.ts");
/* harmony import */ var _book_detail_guard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./book-detail-guard.service */ "./src/app/books/book-detail-guard.service.ts");
/* harmony import */ var _viewbook_viewbook_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viewbook/viewbook.component */ "./src/app/books/viewbook/viewbook.component.ts");
/* harmony import */ var _updatebook_updatebook_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./updatebook/updatebook.component */ "./src/app/books/updatebook/updatebook.component.ts");
/* harmony import */ var _addbook_guard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addbook-guard-service */ "./src/app/books/addbook-guard-service.ts");










const bookRoutes = [
    { path: '', component: _books_component__WEBPACK_IMPORTED_MODULE_2__["BooksComponent"] },
    { path: 'add', component: _addbook_component__WEBPACK_IMPORTED_MODULE_3__["AddBookComponent"],
        canActivate: [_book_detail_guard_service__WEBPACK_IMPORTED_MODULE_4__["BookDetailGuardService"]],
        canDeactivate: [_addbook_guard_service__WEBPACK_IMPORTED_MODULE_7__["AddBookGuardService"]] },
    { path: ':id', component: _viewbook_viewbook_component__WEBPACK_IMPORTED_MODULE_5__["ViewbookComponent"],
        canActivate: [_book_detail_guard_service__WEBPACK_IMPORTED_MODULE_4__["BookDetailGuardService"]] },
    { path: 'update/:id', component: _updatebook_updatebook_component__WEBPACK_IMPORTED_MODULE_6__["UpdatebookComponent"],
        canActivate: [_book_detail_guard_service__WEBPACK_IMPORTED_MODULE_4__["BookDetailGuardService"]] }
];
class BooksRoutingModule {
}
BooksRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: BooksRoutingModule });
BooksRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function BooksRoutingModule_Factory(t) { return new (t || BooksRoutingModule)(); }, imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(bookRoutes)
        ], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BooksRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BooksRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(bookRoutes)
                ],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/books/books.component.ts":
/*!******************************************!*\
  !*** ./src/app/books/books.component.ts ***!
  \******************************************/
/*! exports provided: BooksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksComponent", function() { return BooksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _recent_views_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recent-views.service */ "./src/app/books/recent-views.service.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./books.service */ "./src/app/books/books.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _search_filter_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./search-filter.pipe */ "./src/app/books/search-filter.pipe.ts");









const _c0 = function (a1) { return ["update", a1]; };
function BooksComponent_tbody_89_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "th", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "View");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Update");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const b_r1 = ctx.$implicit;
    const i_r2 = ctx.index;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](i_r2 + 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](b_r1.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](b_r1.quantity);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](b_r1.price);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](b_r1.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", b_r1.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c0, b_r1.id));
} }
class BooksComponent {
    constructor(_recentViews, _authService, _booksservice) {
        this._recentViews = _recentViews;
        this._authService = _authService;
        this._booksservice = _booksservice;
        //paath:string="../../assets/images/3.jpg";
        this.time = new Date();
        this.day = new Date();
        this.views = [];
        setInterval(() => {
            this.time = new Date().toLocaleTimeString();
        }, 1);
        setInterval(() => {
            this.day = new Date().toDateString();
        }, 1);
        this.views = this._recentViews.getviews();
    }
    ngOnInit() {
        console.log("In books.component.ts ");
        this.getBooks();
        //this.views=[this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
        //console.log(this.views);
        // this.getViews(this.books);
        // console.log(this.views);
    }
    getBooks() {
        this._booksservice.getbooks().subscribe((books) => {
            this.books = books;
            //this.views=[this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title],
            //console.log(this.views);
            err => console.log(err);
        });
    }
    search(formvalue) {
        this.searchTerm = formvalue.searchTerm;
        //console.log(this.searchTerm)
    }
}
BooksComponent.ɵfac = function BooksComponent_Factory(t) { return new (t || BooksComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_recent_views_service__WEBPACK_IMPORTED_MODULE_1__["RecentViews"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_books_service__WEBPACK_IMPORTED_MODULE_3__["BooksService"])); };
BooksComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BooksComponent, selectors: [["app-books"]], decls: 124, vars: 13, consts: [["href", "https://fonts.googleapis.com/css?family=Lobster+Two&display=swap", "rel", "stylesheet"], [1, "mainBg"], ["id", "carouselExampleInterval", "data-ride", "carousel", 1, "carousel", "slide"], [1, "carousel-inner"], [1, "fas", "fa-bookmark", "fa-rotate-270", "fa-3x", "bookmark-icon"], ["data-interval", "10000", 1, "carousel-item", "active", 2, "text-align", "center"], [1, "recent"], [1, "views"], ["data-interval", "2000", 1, "carousel-item", 2, "text-align", "center"], ["href", "#carouselExampleInterval", "role", "button", "data-slide", "prev", 1, "carousel-control-prev"], ["aria-hidden", "true", 1, "carousel-control-prev-icon"], [1, "sr-only"], ["href", "#carouselExampleInterval", "role", "button", "data-slide", "next", 1, "carousel-control-next"], ["aria-hidden", "true", 1, "carousel-control-next-icon"], [1, "mainBox"], [1, "welcome"], [1, "welcomeCap"], [1, "timeBox"], [2, "margin-left", "20px"], [1, "additemBox", "col-*-*"], ["routerLink", "add", "type", "button", 1, "btn", "btn-success"], [1, "row"], [1, "title", "col-sm-12"], [1, "search-container"], ["type", "text", "placeholder", "Search..", "name", "searchTerm", 3, "ngModel", "ngModelChange"], ["type", "submit"], [1, "fas", "fa-search", "fa-1x"], [1, "content"], [1, "table", "table-striped", "table-dark"], [1, "w"], [2, "width", "320px"], [2, "width", "200px"], [2, "width", "230px"], ["scope", "col"], ["scope", "col", 2, "text-align", "center"], [4, "ngFor", "ngForOf"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"], ["scope", "row"], [2, "text-align", "center"], ["type", "button", 1, "btn", "btn-info", 3, "routerLink"]], template: function BooksComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "link", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "recent search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "recent search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "recent search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "recent search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "recent search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Previous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Welcome,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, " Have a nice day! ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Books");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Here you can add a new Book....");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Add");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "input", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function BooksComponent_Template_input_ngModelChange_62_listener($event) { return ctx.searchTerm = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "i", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "table", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "colgroup");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "col", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "col", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "col", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "col", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "col", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "col", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "thead");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "th", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "#");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "th", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "th", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Quantity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "th", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "th", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Author");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "th", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Actions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](89, BooksComponent_tbody_89_Template, 17, 9, "tbody", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](90, "searchFilter");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "i", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "BookZone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "a", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "i", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](99, "i", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "a", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "i", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "a", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Mail us at:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, " Example.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, " Box 564, Disneyland");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](112, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, " Banagalore ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Visit us at");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, " BookZone Internet Private Limited,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, " Buildings Alyssa, Begonia &");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, " Clove Embassy Tech Village,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.views[0]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.views[1]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.views[2]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.views[3]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.views[4]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.time);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.day);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchTerm);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.searchTerm, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](90, 10, ctx.books, ctx.searchTerm));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"]], pipes: [_search_filter_pipe__WEBPACK_IMPORTED_MODULE_7__["SearchFilterPipe"]], styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 200vh;\n}\n.carousel-card[_ngcontent-%COMP%]{\n   \n    height: 30px;\n    width: 14rem;\n    background-color: white;\n    position: absolute;\n}\n#carouselExampleInterval[_ngcontent-%COMP%]{\n    width: 19rem;margin-top:7%;\n            margin-left:6%;\n            position: absolute; height: 400px;\n}\n.mainBox[_ngcontent-%COMP%]{\n    position: absolute;\n    height: 61%;\n    width: 65%;\n    background-color: whitesmoke;\n    margin-left:29%;\n    margin-top:7%;\n    border-radius:5px;\n\n}\n.welcome[_ngcontent-%COMP%]{\n    margin-top: 50px;\n    margin-left:30px;\n}\n.welcomeCap[_ngcontent-%COMP%]{\n    font-size:16px;\n    margin-left:30px;\n    font-weight: inherit\n}\n.timeBox[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    width:250px;\n    background-color: #A0DEE2;\n}\n.additemBox[_ngcontent-%COMP%]{\n    position: absolute;\n    height: 150px;\n    width: 300px;\n    background-color: white;\n    margin-left:60%;\n    margin-top:-15%;\n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    padding: 15px;\n}\n.ccc[_ngcontent-%COMP%]{\n    position: absolute;\n    margin-top: 40%;\n   margin-left:6%; \n}\n.content[_ngcontent-%COMP%]{\n    position: absolute;\n    margin-top:38%;\n    margin-left:7%;\n}\ntd[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n    margin: 6px;\n}\ntable[_ngcontent-%COMP%]{\n    overflow-y:scroll; \n    height:600px; \n    display:block; \n    table-layout: fixed;\n    width: 1200px;\n}\n.card-title[_ngcontent-%COMP%]{\n    \n    font-size:30px;\n    color:blue\n}\n.usericon[_ngcontent-%COMP%]{\n    margin-top:10%; \n    margin-left:35%;\n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 0px;\n    width:88.3%;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n    position: absolute;\n    margin-top:32%;\n    margin-left:7%;\n}\n.carousel-item[_ngcontent-%COMP%]{\n    \n    height: 335px;\n    border:12px solid black;\n    border-style: groove;\n    \n    \n}\n.bd-example[_ngcontent-%COMP%]{\n    height: 350px;  \n    \n    width: 10rem;\n    position: absolute;\n}\n.carousel-item[_ngcontent-%COMP%]   .views[_ngcontent-%COMP%]{\n    font-size: 20px ;\n    border-style: solid;\n    margin-top:70px; \n    border-width: 6px;\n    border-style: inset;\n    padding: 12px;\n\n    \n}\n.bookmark-icon[_ngcontent-%COMP%]{\n    padding: 15px;\n    margin-left:-3%; \n    position: absolute;\n    color: yellow\n\n}\n.recent[_ngcontent-%COMP%]{\n    margin-top:5%;\n    margin-left:-40%;\n    border:1px  ;\n    \n    background-color:black;\n    color:white;\n\n}\n.search-container[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    height: 36px;\n    float: right;\n    width:300px;\n    margin:20px;\n    margin-right:10%;\n    font-size:16px;\n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n   \n}\n.search-container[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\n    float: right;\n    margin-right:-30%;\n    height: 34px;\n    margin-top:1.8%;\n    width:38px;\n}\n\n.footer[_ngcontent-%COMP%] {\n   margin-top:90%;\n\n   margin-left:6%;\n    left: 0;\n    height: 100px;\n    bottom: 0;\n    width: 89%;\n    background-color: white;\n   \n    text-align: center;\n    border-radius:5px;\n  }\naddress[_ngcontent-%COMP%]{\n      float: right;\n      font-size: 12px;\n      margin-top:-7%;\n      margin-right:30px;\n      text-align: left;\n  }\n.mailus[_ngcontent-%COMP%]{\n    \n    font-size: 12px;\n    margin-top:-2%;\n    margin-left:63%;\n    padding: 12px;\n    text-align: left;\n  }\n.vl[_ngcontent-%COMP%] {\n    border-left: 1px solid black;\n    height: 12%;\n    margin-right:10%;\n    margin-left:-6%;\n    position: absolute;\n   \n  }\n.footer-icon[_ngcontent-%COMP%]{\n      float: left;\n      margin-top:15px;\n      margin-left:15px;\n  }\n.logo-footer[_ngcontent-%COMP%]{\n    float: left;\n    margin-top:21px;\n    \n    font-size:20px;\n}\n.fb-icon[_ngcontent-%COMP%]{\n    margin-left:-10%;\n}\n#social-fb[_ngcontent-%COMP%]:hover {\n    color: #3B5998;\n}\n#social-tw[_ngcontent-%COMP%]:hover {\n    color: #4099FF;\n}\n#social-gp[_ngcontent-%COMP%]:hover {\n    color: #d34836;\n}\n#social-em[_ngcontent-%COMP%]:hover {\n    color: #f39c12;\n}\n.social-icons[_ngcontent-%COMP%]{\n    float: left;\n    margin-top:20px;\n    margin-left:170px;\n    \n}\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    padding: 10px;\n    color:rgb(90, 201, 238)\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYm9va3MvYm9va3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRDQUE4RDtJQUM5RCxzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLGlCQUFpQjtBQUNyQjtBQUNBOztJQUVJLFlBQVk7SUFDWixZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksWUFBWSxDQUFDLGFBQWE7WUFDbEIsY0FBYztZQUNkLGtCQUFrQixFQUFFLGFBQWE7QUFDN0M7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsVUFBVTtJQUNWLDRCQUE0QjtJQUM1QixlQUFlO0lBQ2YsYUFBYTtJQUNiLGlCQUFpQjs7QUFFckI7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEI7QUFDSjtBQUNBO0lBQ0ksY0FBYztJQUNkLDZCQUE2QjtJQUM3Qiw2Q0FBNkM7SUFDN0MscUJBQXFCO0lBQ3JCLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVztJQUNYLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixlQUFlO0lBQ2YsNkNBQTZDO0lBQzdDLGFBQWE7QUFDakI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixlQUFlO0dBQ2hCLGNBQWM7QUFDakI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztBQUNsQjtBQUNBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsWUFBWTtJQUNaLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsYUFBYTtBQUNqQjtBQUNBO0lBQ0kseUNBQXlDO0lBQ3pDLGNBQWM7SUFDZDtBQUNKO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksY0FBYztJQUNkLDZCQUE2QjtJQUM3Qiw2Q0FBNkM7SUFDN0MscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7O0lBRUksYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixvQkFBb0I7OztBQUd4QjtBQUNBO0lBQ0ksYUFBYTs7SUFFYixZQUFZO0lBQ1osa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLGFBQWE7OztBQUdqQjtBQUNBO0lBQ0ksYUFBYTtJQUNiLGVBQWU7SUFDZixrQkFBa0I7SUFDbEI7O0FBRUo7QUFDQTtJQUNJLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsWUFBWTs7SUFFWixzQkFBc0I7SUFDdEIsV0FBVzs7QUFFZjtBQUNBO0lBQ0ksWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0lBQ1gsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsNkNBQTZDOztBQUVqRDtBQUNBO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osZUFBZTtJQUNmLFVBQVU7QUFDZDtBQUNBLCtCQUErQjtBQUMvQjtHQUNHLGNBQWM7O0dBRWQsY0FBYztJQUNiLE9BQU87SUFDUCxhQUFhO0lBQ2IsU0FBUztJQUNULFVBQVU7SUFDVix1QkFBdUI7O0lBRXZCLGtCQUFrQjtJQUNsQixpQkFBaUI7RUFDbkI7QUFDQTtNQUNJLFlBQVk7TUFDWixlQUFlO01BQ2YsY0FBYztNQUNkLGlCQUFpQjtNQUNqQixnQkFBZ0I7RUFDcEI7QUFDQTs7SUFFRSxlQUFlO0lBQ2YsY0FBYztJQUNkLGVBQWU7SUFDZixhQUFhO0lBQ2IsZ0JBQWdCO0VBQ2xCO0FBQ0E7SUFDRSw0QkFBNEI7SUFDNUIsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCOztFQUVwQjtBQUNBO01BQ0ksV0FBVztNQUNYLGVBQWU7TUFDZixnQkFBZ0I7RUFDcEI7QUFDRjtJQUNJLFdBQVc7SUFDWCxlQUFlOztJQUVmLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixpQkFBaUI7O0FBRXJCO0FBQ0E7SUFDSSxhQUFhO0lBQ2I7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2Jvb2tzL2Jvb2tzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbkJne1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL2Jvb2suanBnKSBuby1yZXBlYXQgY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtby1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tcy1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIG1pbi1oZWlnaHQ6IDIwMHZoO1xufVxuLmNhcm91c2VsLWNhcmR7XG4gICBcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgd2lkdGg6IDE0cmVtO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbiNjYXJvdXNlbEV4YW1wbGVJbnRlcnZhbHtcbiAgICB3aWR0aDogMTlyZW07bWFyZ2luLXRvcDo3JTtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OjYlO1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlOyBoZWlnaHQ6IDQwMHB4O1xufVxuXG4ubWFpbkJveHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiA2MSU7XG4gICAgd2lkdGg6IDY1JTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZXNtb2tlO1xuICAgIG1hcmdpbi1sZWZ0OjI5JTtcbiAgICBtYXJnaW4tdG9wOjclO1xuICAgIGJvcmRlci1yYWRpdXM6NXB4O1xuXG59XG5cbi53ZWxjb21le1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgbWFyZ2luLWxlZnQ6MzBweDtcbn1cbi53ZWxjb21lQ2Fwe1xuICAgIGZvbnQtc2l6ZToxNnB4O1xuICAgIG1hcmdpbi1sZWZ0OjMwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGluaGVyaXRcbn1cbi50aW1lQm94e1xuICAgIGZvbnQtc2l6ZTozMHB4O1xuICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrOyAqL1xuICAgIGJveC1zaGFkb3c6IDAgMTVweCAyNXB4IC0xMHB4IHJnYmEoMCwwLDAsLjM1KTtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgd2lkdGg6MjUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0EwREVFMjtcbn1cbi5hZGRpdGVtQm94e1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIHdpZHRoOiAzMDBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBtYXJnaW4tbGVmdDo2MCU7XG4gICAgbWFyZ2luLXRvcDotMTUlO1xuICAgIGJveC1zaGFkb3c6IDAgMTVweCAyNXB4IC0xMHB4IHJnYmEoMCwwLDAsLjM1KTtcbiAgICBwYWRkaW5nOiAxNXB4O1xufVxuLmNjY3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogNDAlO1xuICAgbWFyZ2luLWxlZnQ6NiU7IFxufVxuLmNvbnRlbnR7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbi10b3A6MzglO1xuICAgIG1hcmdpbi1sZWZ0OjclO1xufVxudGQgYnV0dG9ue1xuICAgIG1hcmdpbjogNnB4O1xufVxudGFibGV7XG4gICAgb3ZlcmZsb3cteTpzY3JvbGw7IFxuICAgIGhlaWdodDo2MDBweDsgXG4gICAgZGlzcGxheTpibG9jazsgXG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTIwMHB4O1xufVxuLmNhcmQtdGl0bGV7XG4gICAgLyogZm9udC1mYW1pbHk6ICdMb2JzdGVyIFR3bycsIGN1cnNpdmU7ICovXG4gICAgZm9udC1zaXplOjMwcHg7XG4gICAgY29sb3I6Ymx1ZVxufVxuLnVzZXJpY29ue1xuICAgIG1hcmdpbi10b3A6MTAlOyBcbiAgICBtYXJnaW4tbGVmdDozNSU7XG59XG4udGl0bGV7XG4gICAgZm9udC1zaXplOjMwcHg7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgYmxhY2s7ICovXG4gICAgYm94LXNoYWRvdzogMCAxNXB4IDI1cHggLTEwcHggcmdiYSgwLDAsMCwuMzUpO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgd2lkdGg6ODguMyU7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQTBERUUyO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tdG9wOjMyJTtcbiAgICBtYXJnaW4tbGVmdDo3JTtcbn1cbi5jYXJvdXNlbC1pdGVte1xuICAgIFxuICAgIGhlaWdodDogMzM1cHg7XG4gICAgYm9yZGVyOjEycHggc29saWQgYmxhY2s7XG4gICAgYm9yZGVyLXN0eWxlOiBncm9vdmU7XG4gICAgXG4gICAgXG59XG4uYmQtZXhhbXBsZXtcbiAgICBoZWlnaHQ6IDM1MHB4OyAgXG4gICAgXG4gICAgd2lkdGg6IDEwcmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5jYXJvdXNlbC1pdGVtIC52aWV3c3tcbiAgICBmb250LXNpemU6IDIwcHggO1xuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgbWFyZ2luLXRvcDo3MHB4OyBcbiAgICBib3JkZXItd2lkdGg6IDZweDtcbiAgICBib3JkZXItc3R5bGU6IGluc2V0O1xuICAgIHBhZGRpbmc6IDEycHg7XG5cbiAgICBcbn1cbi5ib29rbWFyay1pY29ue1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luLWxlZnQ6LTMlOyBcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29sb3I6IHllbGxvd1xuXG59XG4ucmVjZW50e1xuICAgIG1hcmdpbi10b3A6NSU7XG4gICAgbWFyZ2luLWxlZnQ6LTQwJTtcbiAgICBib3JkZXI6MXB4ICA7XG4gICAgXG4gICAgYmFja2dyb3VuZC1jb2xvcjpibGFjaztcbiAgICBjb2xvcjp3aGl0ZTtcblxufVxuLnNlYXJjaC1jb250YWluZXIgIGlucHV0e1xuICAgIGhlaWdodDogMzZweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgd2lkdGg6MzAwcHg7XG4gICAgbWFyZ2luOjIwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OjEwJTtcbiAgICBmb250LXNpemU6MTZweDtcbiAgICBib3gtc2hhZG93OiAwIDE1cHggMjVweCAtMTBweCByZ2JhKDAsMCwwLC4zNSk7XG4gICBcbn1cbi5zZWFyY2gtY29udGFpbmVyICBidXR0b257XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi1yaWdodDotMzAlO1xuICAgIGhlaWdodDogMzRweDtcbiAgICBtYXJnaW4tdG9wOjEuOCU7XG4gICAgd2lkdGg6MzhweDtcbn1cbi8qLS0tLS0tLS0tLS0tLWZvb3Rlci0tLS0tLS0tLSovXG4uZm9vdGVyIHtcbiAgIG1hcmdpbi10b3A6OTAlO1xuXG4gICBtYXJnaW4tbGVmdDo2JTtcbiAgICBsZWZ0OiAwO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpZHRoOiA4OSU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm9yZGVyLXJhZGl1czo1cHg7XG4gIH1cbiAgYWRkcmVzc3tcbiAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIG1hcmdpbi10b3A6LTclO1xuICAgICAgbWFyZ2luLXJpZ2h0OjMwcHg7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB9XG4gIC5tYWlsdXN7XG4gICAgXG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbi10b3A6LTIlO1xuICAgIG1hcmdpbi1sZWZ0OjYzJTtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLnZsIHtcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkIGJsYWNrO1xuICAgIGhlaWdodDogMTIlO1xuICAgIG1hcmdpbi1yaWdodDoxMCU7XG4gICAgbWFyZ2luLWxlZnQ6LTYlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgIFxuICB9XG4gIC5mb290ZXItaWNvbntcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgbWFyZ2luLXRvcDoxNXB4O1xuICAgICAgbWFyZ2luLWxlZnQ6MTVweDtcbiAgfVxuLmxvZ28tZm9vdGVye1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi10b3A6MjFweDtcbiAgICBcbiAgICBmb250LXNpemU6MjBweDtcbn1cbi5mYi1pY29ue1xuICAgIG1hcmdpbi1sZWZ0Oi0xMCU7XG59XG4jc29jaWFsLWZiOmhvdmVyIHtcbiAgICBjb2xvcjogIzNCNTk5ODtcbn1cbiNzb2NpYWwtdHc6aG92ZXIge1xuICAgIGNvbG9yOiAjNDA5OUZGO1xufVxuI3NvY2lhbC1ncDpob3ZlciB7XG4gICAgY29sb3I6ICNkMzQ4MzY7XG59XG4jc29jaWFsLWVtOmhvdmVyIHtcbiAgICBjb2xvcjogI2YzOWMxMjtcbn1cbi5zb2NpYWwtaWNvbnN7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OjE3MHB4O1xuICAgIFxufVxuLnNvY2lhbC1pY29ucyBhe1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgY29sb3I6cmdiKDkwLCAyMDEsIDIzOClcbn1cblxuIFxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BooksComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-books',
                templateUrl: './books.component.html',
                styleUrls: ['./books.component.css']
            }]
    }], function () { return [{ type: _recent_views_service__WEBPACK_IMPORTED_MODULE_1__["RecentViews"] }, { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }, { type: _books_service__WEBPACK_IMPORTED_MODULE_3__["BooksService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/books/books.module.ts":
/*!***************************************!*\
  !*** ./src/app/books/books.module.ts ***!
  \***************************************/
/*! exports provided: BooksModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksModule", function() { return BooksModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _books_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./books-routing.module */ "./src/app/books/books-routing.module.ts");
/* harmony import */ var _books_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./books.component */ "./src/app/books/books.component.ts");
/* harmony import */ var _addbook_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addbook.component */ "./src/app/books/addbook.component.ts");
/* harmony import */ var _book_detail_guard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./book-detail-guard.service */ "./src/app/books/book-detail-guard.service.ts");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./books.service */ "./src/app/books/books.service.ts");
/* harmony import */ var _viewbook_viewbook_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./viewbook/viewbook.component */ "./src/app/books/viewbook/viewbook.component.ts");
/* harmony import */ var _updatebook_updatebook_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./updatebook/updatebook.component */ "./src/app/books/updatebook/updatebook.component.ts");
/* harmony import */ var _addbook_guard_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./addbook-guard-service */ "./src/app/books/addbook-guard-service.ts");
/* harmony import */ var _recent_views_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./recent-views.service */ "./src/app/books/recent-views.service.ts");
/* harmony import */ var _search_filter_pipe__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./search-filter.pipe */ "./src/app/books/search-filter.pipe.ts");













class BooksModule {
}
BooksModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: BooksModule });
BooksModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function BooksModule_Factory(t) { return new (t || BooksModule)(); }, providers: [_books_service__WEBPACK_IMPORTED_MODULE_6__["BooksService"], _book_detail_guard_service__WEBPACK_IMPORTED_MODULE_5__["BookDetailGuardService"], _addbook_guard_service__WEBPACK_IMPORTED_MODULE_9__["AddBookGuardService"], _recent_views_service__WEBPACK_IMPORTED_MODULE_10__["RecentViews"]], imports: [[
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__["SharedModule"],
            _books_routing_module__WEBPACK_IMPORTED_MODULE_2__["BooksRoutingModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BooksModule, { declarations: [_books_component__WEBPACK_IMPORTED_MODULE_3__["BooksComponent"], _addbook_component__WEBPACK_IMPORTED_MODULE_4__["AddBookComponent"], _viewbook_viewbook_component__WEBPACK_IMPORTED_MODULE_7__["ViewbookComponent"], _updatebook_updatebook_component__WEBPACK_IMPORTED_MODULE_8__["UpdatebookComponent"], _search_filter_pipe__WEBPACK_IMPORTED_MODULE_11__["SearchFilterPipe"]], imports: [_shared_shared_module__WEBPACK_IMPORTED_MODULE_1__["SharedModule"],
        _books_routing_module__WEBPACK_IMPORTED_MODULE_2__["BooksRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BooksModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__["SharedModule"],
                    _books_routing_module__WEBPACK_IMPORTED_MODULE_2__["BooksRoutingModule"],
                ],
                declarations: [_books_component__WEBPACK_IMPORTED_MODULE_3__["BooksComponent"], _addbook_component__WEBPACK_IMPORTED_MODULE_4__["AddBookComponent"], _viewbook_viewbook_component__WEBPACK_IMPORTED_MODULE_7__["ViewbookComponent"], _updatebook_updatebook_component__WEBPACK_IMPORTED_MODULE_8__["UpdatebookComponent"], _search_filter_pipe__WEBPACK_IMPORTED_MODULE_11__["SearchFilterPipe"]],
                providers: [_books_service__WEBPACK_IMPORTED_MODULE_6__["BooksService"], _book_detail_guard_service__WEBPACK_IMPORTED_MODULE_5__["BookDetailGuardService"], _addbook_guard_service__WEBPACK_IMPORTED_MODULE_9__["AddBookGuardService"], _recent_views_service__WEBPACK_IMPORTED_MODULE_10__["RecentViews"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/books/books.service.ts":
/*!****************************************!*\
  !*** ./src/app/books/books.service.ts ***!
  \****************************************/
/*! exports provided: BooksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksService", function() { return BooksService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




class BooksService {
    constructor(_http) {
        this._http = _http;
        this._booksUrl = "http://booksjson:3200/books";
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
    }
    getbooks() {
        return this._http.get(this._booksUrl);
    }
    addbook(newBook) {
        console.log(newBook);
        return this._http.post(this._booksUrl, newBook, this.httpOptions);
    }
    viewbook(id) {
        //console.log(id)
        let viewbookURL = `${this._booksUrl}/${id}`;
        return this._http.get(viewbookURL);
    }
    updatebook(updatedBook, id) {
        let viewbookURL = `${this._booksUrl}/${id}`;
        return this._http.put(viewbookURL, updatedBook, this.httpOptions);
    }
    deletebook(bookid) {
        let deletebookURL = `${this._booksUrl}/${bookid}`;
        return this._http.delete(deletebookURL);
    }
}
BooksService.ɵfac = function BooksService_Factory(t) { return new (t || BooksService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
BooksService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: BooksService, factory: BooksService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BooksService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/books/recent-views.service.ts":
/*!***********************************************!*\
  !*** ./src/app/books/recent-views.service.ts ***!
  \***********************************************/
/*! exports provided: RecentViews */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecentViews", function() { return RecentViews; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./books.service */ "./src/app/books/books.service.ts");



class RecentViews {
    constructor(_booksService) {
        this._booksService = _booksService;
        this.recentView = [];
        this.books = [];
        this.getviewsbybooks();
        // console.log(this.books)
        //this.recentView= [this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
    }
    getviews() {
        console.log(this.recentView);
        return this.recentView;
    }
    setviews(name) {
        console.log(this.recentView);
        if (this.recentView.length < 5) {
            if (this.recentView.indexOf(name) !== -1) {
                console.log(" EXISTS" + name);
            }
            else {
                this.recentView.push(name);
            }
        }
        else {
            this.recentView.shift();
            if (this.recentView.indexOf(name) !== -1) {
                console.log("EXISTS" + name);
            }
            else {
                this.recentView.push(name);
            }
        }
    }
    getviewsbybooks() {
        this._booksService.getbooks().subscribe((books) => {
            this.books = books,
                this.recentView = [this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
            console.log(this.recentView);
            err => console.log(err);
        });
        //console.log(this.recentView)
    }
}
RecentViews.ɵfac = function RecentViews_Factory(t) { return new (t || RecentViews)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"])); };
RecentViews.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: RecentViews, factory: RecentViews.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RecentViews, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/books/search-filter.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/books/search-filter.pipe.ts ***!
  \*********************************************/
/*! exports provided: SearchFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFilterPipe", function() { return SearchFilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class SearchFilterPipe {
    transform(books, searchTerm) {
        if (!books || !searchTerm) {
            return books;
        }
        //  console.log(searchTerm.toLowerCase() + "   "+book.title.toLowerCase())
        return books.filter(book => 
        // console.log(searchTerm.toLowerCase() + "   "+book.title.toLowerCase())
        book.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
    }
}
SearchFilterPipe.ɵfac = function SearchFilterPipe_Factory(t) { return new (t || SearchFilterPipe)(); };
SearchFilterPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "searchFilter", type: SearchFilterPipe, pure: true });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchFilterPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'searchFilter'
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/books/updatebook/updatebook.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/books/updatebook/updatebook.component.ts ***!
  \**********************************************************/
/*! exports provided: UpdatebookComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatebookComponent", function() { return UpdatebookComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../books.service */ "./src/app/books/books.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");





class UpdatebookComponent {
    constructor(_booksservice, route, router) {
        this._booksservice = _booksservice;
        this.route = route;
        this.router = router;
        this.book = {};
    }
    ngOnInit() {
        this.route.params.forEach((params) => {
            this.id = +params['id'];
            //console.log(params)
        });
        this.updateBook(this.id);
    }
    updateBook(id) {
        this._booksservice.viewbook(id).subscribe((data) => {
            this.book = data;
            //console.log(this.book)
        });
    }
    saveChanges(formValue) {
        const updatedBook = formValue;
        this._booksservice.updatebook(updatedBook, this.id).subscribe((data) => this.router.navigate(['books']));
    }
    deleteBook(bookid) {
        //console.log(bookid)
        this._booksservice.deletebook(bookid).subscribe((data) => {
            //console.log(data)
            this.router.navigate(['books']);
        });
    }
}
UpdatebookComponent.ɵfac = function UpdatebookComponent_Factory(t) { return new (t || UpdatebookComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
UpdatebookComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: UpdatebookComponent, selectors: [["app-updatebook"]], decls: 79, vars: 8, consts: [[1, "mainBg"], [1, "addbookform"], [1, "title"], ["id", "collapsible", 3, "ngSubmit"], ["addForm", "ngForm"], [1, "form-group"], ["for", "title"], ["type", "text", "name", "title", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "ngModel", "ngModelChange"], ["titleRef", "ngModel"], [1, "form-group", 2, "margin-left", "45%", "margin-top", "-8.5%"], ["type", "text", "name", "subtitle", 1, "form-control", "form-control-sm", 2, "width", "100%", 3, "ngModel", "ngModelChange"], ["subtitleRef", "ngModel"], ["for", "exampleFormControlTextarea2"], ["id", "exampleFormControlTextarea2", "rows", "3", "name", "description", 1, "form-control", "rounded-0", 2, "height", "200px", 3, "ngModel", "ngModelChange"], ["descriptionRef", "ngModel"], ["for", "author"], ["type", "text", "name", "author", 1, "form-control", "form-control-sm", 2, "width", "30%", 3, "ngModel", "ngModelChange"], ["authorRef", "ngModel"], [1, "form-group", 2, "margin-left", "35%", "margin-top", "-8.5%"], ["for", "quantity"], ["type", "text", "name", "category", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "ngModel", "ngModelChange"], ["categoryRef", "ngModel"], ["for", "price"], ["type", "number", "name", "price", 1, "form-control", "form-control-sm", 2, "width", "30%", 3, "ngModel", "ngModelChange"], ["priceRef", "ngModel"], ["type", "number", "name", "quantity", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "ngModel", "ngModelChange"], ["quantityRef", "ngModel"], ["type", "submit", 1, "btn", "btn-primary"], ["type", "button", 1, "btn", "btn-danger", 2, "float", "right", 3, "click"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"]], template: function UpdatebookComponent_Template(rf, ctx) { if (rf & 1) {
        const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function UpdatebookComponent_Template_form_ngSubmit_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5); return ctx.saveChanges(_r0.value); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "input", 7, 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_9_listener($event) { return ctx.book.title = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Subtitle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 10, 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_14_listener($event) { return ctx.book.subtitle = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Description");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "textarea", 13, 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_textarea_ngModelChange_19_listener($event) { return ctx.book.description = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Author");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 16, 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_24_listener($event) { return ctx.book.author = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "input", 20, 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_29_listener($event) { return ctx.book.category = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "label", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "input", 23, 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_34_listener($event) { return ctx.book.price = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "label", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Quantity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "input", 25, 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UpdatebookComponent_Template_input_ngModelChange_39_listener($event) { return ctx.book.quantity = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "save");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UpdatebookComponent_Template_button_click_44_listener() { return ctx.deleteBook(ctx.book.id); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Delete");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "i", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "BookZone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "a", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "i", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "i", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "i", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "a", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "i", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Mail us at:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, " Example.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Box 564, Disneyland");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, " Bangalore ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Visit us at");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, " BookZone Internet Private Limited,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, " Buildings Alyssa, Begonia &");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, " Clove Embassy Tech Village,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](78, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.book.title, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.subtitle);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.description);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.author);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.category);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.price);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.book.quantity);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NumberValueAccessor"]], styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 100vh;\n    overflow:hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:100px;\n    margin-left:17%;\n    position: absolute;\n    height: 100%;\n    width: 70%;\n    scroll-behavior: auto;\n    \n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n}\nform[_ngcontent-%COMP%]{\n    padding: 15px;\n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:65%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\naddress[_ngcontent-%COMP%]{\n    float: right;\n    font-size: 12px;\n    margin-top:-7%;\n    margin-right:30px;\n    text-align: left;\n}\n.mailus[_ngcontent-%COMP%]{\n  font-size: 12px;\n  margin-top:-2%;\n padding: 12px;\n  margin-left:63%;\n  text-align: left;\n}\n.vl[_ngcontent-%COMP%] {\n  border-left: 1px solid black;\n  height: 83px;\n  margin-right:10%;\n  margin-left:-6%;\n  position: absolute;\n  margin-top:0px;\n}\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYm9va3MvdXBkYXRlYm9vay91cGRhdGVib29rLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0Q0FBaUU7SUFDakUsc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QiwyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksdUJBQXVCO0lBQ3ZCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixVQUFVO0lBQ1YscUJBQXFCOztBQUV6QjtBQUNBO0lBQ0ksY0FBYztJQUNkLDZCQUE2QjtJQUM3Qiw2Q0FBNkM7SUFDN0MscUJBQXFCO0lBQ3JCLGFBQWE7SUFDYixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUdBOztJQUVJLGNBQWM7O0tBRWIsY0FBYztLQUNkLGlCQUFpQjtLQUNqQixhQUFhO0tBQ2IsU0FBUztLQUNULFVBQVU7SUFDWCxrQkFBa0I7S0FDakIsdUJBQXVCO1FBQ3BCLGtCQUFrQjtLQUNyQixrQkFBa0I7S0FDbEIsaUJBQWlCO0dBQ25CO0FBQ0E7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztDQUNmLGFBQWE7RUFDWixlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSw0QkFBNEI7RUFDNUIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7QUFDaEI7QUFDRztPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCO0FBQ0Y7S0FDSSxXQUFXO0tBQ1gsZUFBZTs7S0FFZixjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxnQkFBZ0I7Q0FDcEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLFdBQVc7S0FDWCxlQUFlO0tBQ2YsaUJBQWlCOztDQUVyQjtBQUNBO0tBQ0ksYUFBYTtLQUNiO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9ib29rcy91cGRhdGVib29rL3VwZGF0ZWJvb2suY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluQmd7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvYm9vay5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG4gICAgb3ZlcmZsb3c6aGlkZGVuO1xufVxuLmFkZGJvb2tmb3Jte1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6MTAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6MTclO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBzY3JvbGwtYmVoYXZpb3I6IGF1dG87XG4gICAgXG59XG4udGl0bGV7XG4gICAgZm9udC1zaXplOjMwcHg7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgYmxhY2s7ICovXG4gICAgYm94LXNoYWRvdzogMCAxNXB4IDI1cHggLTEwcHggcmdiYSgwLDAsMCwuMzUpO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGhlaWdodDogNzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0EwREVFMjtcbn1cbmZvcm17XG4gICAgcGFkZGluZzogMTVweDtcbn1cblxuXG4uZm9vdGVyIHtcbiBcbiAgICBtYXJnaW4tbGVmdDo2JTtcbiAgICBcbiAgICAgbWFyZ2luLXRvcDo2NSU7XG4gICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICBib3R0b206IDA7XG4gICAgIHdpZHRoOiA4OSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGJvcmRlci1yYWRpdXM6NXB4O1xuICAgfVxuICAgYWRkcmVzc3tcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbi10b3A6LTclO1xuICAgIG1hcmdpbi1yaWdodDozMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ubWFpbHVze1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6LTIlO1xuIHBhZGRpbmc6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OjYzJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi52bCB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgYmxhY2s7XG4gIGhlaWdodDogODNweDtcbiAgbWFyZ2luLXJpZ2h0OjEwJTtcbiAgbWFyZ2luLWxlZnQ6LTYlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6MHB4O1xufVxuICAgLmZvb3Rlci1pY29ue1xuICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgIG1hcmdpbi10b3A6MTVweDtcbiAgICAgICBtYXJnaW4tbGVmdDoxNXB4O1xuICAgfVxuIC5sb2dvLWZvb3RlcntcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIG1hcmdpbi10b3A6MjFweDtcbiAgICAgXG4gICAgIGZvbnQtc2l6ZToyMHB4O1xuIH1cbiAuZmItaWNvbntcbiAgICAgbWFyZ2luLWxlZnQ6LTEwJTtcbiB9XG4gI3NvY2lhbC1mYjpob3ZlciB7XG4gICAgIGNvbG9yOiAjM0I1OTk4O1xuIH1cbiAjc29jaWFsLXR3OmhvdmVyIHtcbiAgICAgY29sb3I6ICM0MDk5RkY7XG4gfVxuICNzb2NpYWwtZ3A6aG92ZXIge1xuICAgICBjb2xvcjogI2QzNDgzNjtcbiB9XG4gI3NvY2lhbC1lbTpob3ZlciB7XG4gICAgIGNvbG9yOiAjZjM5YzEyO1xuIH1cbiAuc29jaWFsLWljb25ze1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICBtYXJnaW4tbGVmdDoxNzBweDtcbiAgICAgXG4gfVxuIC5zb2NpYWwtaWNvbnMgYXtcbiAgICAgcGFkZGluZzogMTBweDtcbiAgICAgY29sb3I6cmdiKDkwLCAyMDEsIDIzOClcbiB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UpdatebookComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-updatebook',
                templateUrl: './updatebook.component.html',
                styleUrls: ['./updatebook.component.css']
            }]
    }], function () { return [{ type: _books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/books/viewbook/viewbook.component.ts":
/*!******************************************************!*\
  !*** ./src/app/books/viewbook/viewbook.component.ts ***!
  \******************************************************/
/*! exports provided: ViewbookComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewbookComponent", function() { return ViewbookComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _books_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../books.service */ "./src/app/books/books.service.ts");
/* harmony import */ var _recent_views_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../recent-views.service */ "./src/app/books/recent-views.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");







class ViewbookComponent {
    constructor(_booksservice, _recentViews, route, location) {
        this._booksservice = _booksservice;
        this._recentViews = _recentViews;
        this.route = route;
        this.location = location;
        this.book = {};
    }
    ngOnInit() {
        this.route.params.forEach((params) => {
            this.id = +params['id'];
        });
        this.viewbook(this.id);
    }
    goBack() {
        this.location.back();
    }
    viewbook(id) {
        this._booksservice.viewbook(id).subscribe((data) => {
            this.book = data;
            //  console.log(this.book)
            //console.log(this.book)
            this._recentViews.setviews(this.book.title);
        });
    }
}
ViewbookComponent.ɵfac = function ViewbookComponent_Factory(t) { return new (t || ViewbookComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_recent_views_service__WEBPACK_IMPORTED_MODULE_2__["RecentViews"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"])); };
ViewbookComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ViewbookComponent, selectors: [["app-viewbook"]], decls: 70, vars: 8, consts: [[1, "mainBg"], [1, "addbookform"], [1, "title"], ["id", "collapsible", 3, "ngSubmit"], ["addForm", "ngForm"], [1, "form-group"], ["for", "title"], ["type", "text", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "value"], [1, "form-group", 2, "margin-left", "45%", "margin-top", "-8.5%"], ["type", "text", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "100%", 3, "value"], ["for", "exampleFormControlTextarea2"], ["id", "exampleFormControlTextarea2", "rows", "3", "name", "description", "readonly", "", 1, "form-control", "rounded-0", 2, "height", "200px", 3, "value"], ["for", "author"], ["type", "text", "name", "author", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "30%", 3, "value"], [1, "form-group", 2, "margin-left", "35%", "margin-top", "-8.5%"], ["for", "quantity"], ["type", "text", "name", "category", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "value"], ["for", "price"], ["type", "number", "name", "price", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "30%", 3, "value"], ["type", "number", "name", "quantity", "readonly", "", 1, "form-control", "form-control-sm", 2, "width", "40%", 3, "value"], ["type", "submit", 1, "btn", "btn-primary"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"]], template: function ViewbookComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ViewbookComponent_Template_form_ngSubmit_4_listener() { return ctx.goBack(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Subtitle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Description");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "textarea", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Author");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Quantity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "GoBack");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "BookZone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "i", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "i", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "a", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "i", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "a", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "i", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Mail us at:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, " Example.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, " Box 564, Disneyland");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, " Bangalore ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Visit us at");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, " BookZone Internet Private Limited,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Buildings Alyssa, Begonia &");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, " Clove Embassy Tech Village,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.book.title, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.subtitle);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.description);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.author);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.category);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.price);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.book.quantity);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"]], styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 100vh;\n    overflow:hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:100px;\n    margin-left:17%;\n    position: absolute;\n    \n    width: 70%;\n    \n    \n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n}\nform[_ngcontent-%COMP%]{\n    padding: 15px;\n        \n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:65%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\naddress[_ngcontent-%COMP%]{\n       float: right;\n       font-size: 12px;\n       margin-top:-7%;\n       margin-right:30px;\n       text-align: left;\n   }\n.mailus[_ngcontent-%COMP%]{\n     font-size: 12px;\n     margin-top:-2%;\n    padding: 12px;\n     margin-left:63%;\n     text-align: left;\n   }\n.vl[_ngcontent-%COMP%] {\n     border-left: 1px solid black;\n     height: 83px;\n     margin-right:10%;\n     margin-left:-6%;\n     position: absolute;\n     margin-top:0px;\n   }\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYm9va3Mvdmlld2Jvb2svdmlld2Jvb2suY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRDQUFpRTtJQUNqRSxzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLFVBQVU7OztBQUdkO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsNkJBQTZCO0lBQzdCLDZDQUE2QztJQUM3QyxxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSxhQUFhOztBQUVqQjtBQUVBOztJQUVJLGNBQWM7O0tBRWIsY0FBYztLQUNkLGlCQUFpQjtLQUNqQixhQUFhO0tBQ2IsU0FBUztLQUNULFVBQVU7SUFDWCxrQkFBa0I7S0FDakIsdUJBQXVCO1FBQ3BCLGtCQUFrQjtLQUNyQixrQkFBa0I7S0FDbEIsaUJBQWlCO0dBQ25CO0FBQ0E7T0FDSSxZQUFZO09BQ1osZUFBZTtPQUNmLGNBQWM7T0FDZCxpQkFBaUI7T0FDakIsZ0JBQWdCO0dBQ3BCO0FBQ0E7S0FDRSxlQUFlO0tBQ2YsY0FBYztJQUNmLGFBQWE7S0FDWixlQUFlO0tBQ2YsZ0JBQWdCO0dBQ2xCO0FBQ0E7S0FDRSw0QkFBNEI7S0FDNUIsWUFBWTtLQUNaLGdCQUFnQjtLQUNoQixlQUFlO0tBQ2Ysa0JBQWtCO0tBQ2xCLGNBQWM7R0FDaEI7QUFDQTtPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCO0FBQ0Y7S0FDSSxXQUFXO0tBQ1gsZUFBZTs7S0FFZixjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxnQkFBZ0I7Q0FDcEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLFdBQVc7S0FDWCxlQUFlO0tBQ2YsaUJBQWlCOztDQUVyQjtBQUNBO0tBQ0ksYUFBYTtLQUNiO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9ib29rcy92aWV3Ym9vay92aWV3Ym9vay5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW5CZ3tcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rLmpwZykgbm8tcmVwZWF0IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW8tYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbXMtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcbiAgICBvdmVyZmxvdzpoaWRkZW47XG59XG4uYWRkYm9va2Zvcm17XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDoxMDBweDtcbiAgICBtYXJnaW4tbGVmdDoxNyU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8qIGhlaWdodDogODglOyAqL1xuICAgIHdpZHRoOiA3MCU7XG4gICAgXG4gICAgXG59XG4udGl0bGV7XG4gICAgZm9udC1zaXplOjMwcHg7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgYmxhY2s7ICovXG4gICAgYm94LXNoYWRvdzogMCAxNXB4IDI1cHggLTEwcHggcmdiYSgwLDAsMCwuMzUpO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGhlaWdodDogNzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0EwREVFMjtcbn1cbmZvcm17XG4gICAgcGFkZGluZzogMTVweDtcbiAgICAgICAgXG59XG5cbi5mb290ZXIge1xuIFxuICAgIG1hcmdpbi1sZWZ0OjYlO1xuICAgIFxuICAgICBtYXJnaW4tdG9wOjY1JTtcbiAgICAgbWFyZ2luLWJvdHRvbTogMiU7XG4gICAgIGhlaWdodDogMTAwcHg7XG4gICAgIGJvdHRvbTogMDtcbiAgICAgd2lkdGg6IDg5JTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgYm9yZGVyLXJhZGl1czo1cHg7XG4gICB9XG4gICBhZGRyZXNze1xuICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgbWFyZ2luLXRvcDotNyU7XG4gICAgICAgbWFyZ2luLXJpZ2h0OjMwcHg7XG4gICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgIH1cbiAgIC5tYWlsdXN7XG4gICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgbWFyZ2luLXRvcDotMiU7XG4gICAgcGFkZGluZzogMTJweDtcbiAgICAgbWFyZ2luLWxlZnQ6NjMlO1xuICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgfVxuICAgLnZsIHtcbiAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCBibGFjaztcbiAgICAgaGVpZ2h0OiA4M3B4O1xuICAgICBtYXJnaW4tcmlnaHQ6MTAlO1xuICAgICBtYXJnaW4tbGVmdDotNiU7XG4gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgbWFyZ2luLXRvcDowcHg7XG4gICB9XG4gICAuZm9vdGVyLWljb257XG4gICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgbWFyZ2luLXRvcDoxNXB4O1xuICAgICAgIG1hcmdpbi1sZWZ0OjE1cHg7XG4gICB9XG4gLmxvZ28tZm9vdGVye1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMXB4O1xuICAgICBcbiAgICAgZm9udC1zaXplOjIwcHg7XG4gfVxuIC5mYi1pY29ue1xuICAgICBtYXJnaW4tbGVmdDotMTAlO1xuIH1cbiAjc29jaWFsLWZiOmhvdmVyIHtcbiAgICAgY29sb3I6ICMzQjU5OTg7XG4gfVxuICNzb2NpYWwtdHc6aG92ZXIge1xuICAgICBjb2xvcjogIzQwOTlGRjtcbiB9XG4gI3NvY2lhbC1ncDpob3ZlciB7XG4gICAgIGNvbG9yOiAjZDM0ODM2O1xuIH1cbiAjc29jaWFsLWVtOmhvdmVyIHtcbiAgICAgY29sb3I6ICNmMzljMTI7XG4gfVxuIC5zb2NpYWwtaWNvbnN7XG4gICAgIGZsb2F0OiBsZWZ0O1xuICAgICBtYXJnaW4tdG9wOjIwcHg7XG4gICAgIG1hcmdpbi1sZWZ0OjE3MHB4O1xuICAgICBcbiB9XG4gLnNvY2lhbC1pY29ucyBhe1xuICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICBjb2xvcjpyZ2IoOTAsIDIwMSwgMjM4KVxuIH0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ViewbookComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-viewbook',
                templateUrl: './viewbook.component.html',
                styleUrls: ['./viewbook.component.css']
            }]
    }], function () { return [{ type: _books_service__WEBPACK_IMPORTED_MODULE_1__["BooksService"] }, { type: _recent_views_service__WEBPACK_IMPORTED_MODULE_2__["RecentViews"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }, { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }]; }, null); })();


/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");




class SharedModule {
}
SharedModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SharedModule });
SharedModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
        ], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SharedModule, { imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]], exports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SharedModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
                ],
                exports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=books-books-module-es2015.js.map