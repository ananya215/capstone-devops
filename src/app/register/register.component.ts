import { Component, OnInit,ViewEncapsulation } from '@angular/core';

import{User} from '../users/User';
import{UserService } from '../users/user.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

 
  constructor(private _registerService : UserService, private router:Router) { }

  ngOnInit() {
  }
  locations:string[]=[
    "Mumbai", "Pune","Bangalore","Kochi","Gurgram","Chennai","Hyderabad"
  ]
  register(formValue:any){
    const newUser:User ={fname:formValue.fname,
                      lname:formValue.lname,
                      email:formValue.email,
                      password:formValue.password,
                      contact:formValue.contactno,
                      location:formValue.location
                    };
    

    this._registerService.addUser(newUser).subscribe(
      (user:User) => {
      // console.log(user);
      // localStorage.setItem('currentUser', JSON.stringify(user));
      this.router.navigate(['/signin']);

    })

  }
  
}



