import {PipeTransform, Pipe} from '@angular/core';

import{Book} from './book';

@Pipe({
    name:'searchFilter'
})
export class SearchFilterPipe implements PipeTransform{
    transform(books:Book[], searchTerm:string): Book[]{
        if(!books || !searchTerm){
            return books;
        }
      //  console.log(searchTerm.toLowerCase() + "   "+book.title.toLowerCase())
        return books.filter(book => 
            
               // console.log(searchTerm.toLowerCase() + "   "+book.title.toLowerCase())
                book.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
               
            );
            
    }
}