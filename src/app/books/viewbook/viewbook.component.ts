import { Component, OnInit } from '@angular/core';
import{BooksService} from'../books.service';
import{Book} from '../book';
import{Router,ActivatedRoute, Params } from '@angular/router'
import { Location } from "@angular/common";
import { RecentViews } from '../recent-views.service';

@Component({
  selector: 'app-viewbook',
  templateUrl: './viewbook.component.html',
  styleUrls: ['./viewbook.component.css']
})
export class ViewbookComponent implements OnInit {
  id:number;
  book:any={};

  constructor( private _booksservice:BooksService,private _recentViews:RecentViews, private route: ActivatedRoute, private location: Location) { 
  
   

  }
  
  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id = +params['id'];
    
  });

  
    
    

  this.viewbook(this.id);

  
  }
 

  goBack(): void {
    this.location.back();
  }

  viewbook(id){
    this._booksservice.viewbook(id).subscribe(
      (data:any) =>{ this.book=data;
      //  console.log(this.book)
      //console.log(this.book)
      this._recentViews.setviews(this.book.title);
      }
    );

    

  }


  
 
}
