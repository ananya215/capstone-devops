import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }   from '@angular/forms';

import { ViewbookComponent } from './viewbook.component';
import { RecentViews } from '../recent-views.service';
import { BooksService } from '../books.service';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';

describe('ViewbookComponent', () => {
  let component: ViewbookComponent;
  let fixture: ComponentFixture<ViewbookComponent>;
  let recentViewsStub: Partial<RecentViews>;
 let booksServiceStub: Partial<BooksService>;
 let recentViews;
 let bookService;
let c= 4;

  beforeEach(() => {
    
    
    
  recentViewsStub={  
    setviews: function(name){
      const arr1 = ["Sashi","Ramana","RajaRaman"];
    // arr.push(name);
      //console.log(arr1)
      return arr1.push(name)
    }
  };
  booksServiceStub={   
    viewbook(id){
      const arr1 ={3:"fdgdzty",4:"Rachana"}
   
      return id ? of(arr1[id]) : of("undefinedID")  ;
    }
  }
    
  TestBed.configureTestingModule({
    declarations: [ ViewbookComponent ],
    imports:[FormsModule,RouterModule.forRoot([])],
    providers:[
      {provide:RecentViews, useValue:recentViewsStub },
      {provide:BooksService, useValue:booksServiceStub }
      
       
    ]
  });

    fixture = TestBed.createComponent(ViewbookComponent);
    component = fixture.componentInstance;
   // fixture.detectChanges();
   recentViews = fixture.debugElement.injector.get(RecentViews); 
     bookService = fixture.debugElement.injector.get(BooksService);  /* Injector for Service */ 
  });

  it('should create app', () => {
    const result1 = bookService.viewbook(3)
    const result2 = recentViews.setviews("Shashiii")
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it(`should get the id and return the associated name`,()=>{
      const result1 = bookService.viewbook(4)
    
     fixture.detectChanges();
     //console.log(result1,"Viewbook")
    // result1.subscribe(val=>console.log(val))
     result1.subscribe(val=>expect(val).toEqual('Rachana'))
    
  })
  it(`should add the name to the list`,()=>{
    
   const result2 = recentViews.setviews("Shashiii")
   fixture.detectChanges();
    expect(result2).toEqual(4)
  })
});
