import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import{BooksRoutingModule} from './books-routing.module'

import{BooksComponent} from './books.component';
import { AddBookComponent } from './addbook.component';
import{BookDetailGuardService} from './book-detail-guard.service'
import { BooksService } from './books.service';
import { ViewbookComponent } from './viewbook/viewbook.component';
import { UpdatebookComponent } from './updatebook/updatebook.component';
import { AddBookGuardService } from './addbook-guard-service';
import{RecentViews} from './recent-views.service';
import{SearchFilterPipe} from './search-filter.pipe';



@NgModule({
  imports: [
    SharedModule,
    BooksRoutingModule,
    
  ],
  declarations: [BooksComponent,AddBookComponent, ViewbookComponent, UpdatebookComponent,SearchFilterPipe],
  providers:[BooksService,BookDetailGuardService,AddBookGuardService,RecentViews]
})
export class BooksModule { 
  
 
}
