import { Injectable } from '@angular/core'
import { CanDeactivate, ActivatedRouteSnapshot } from '@angular/router'
import { AddBookComponent } from './addbook.component';

@Injectable()
export class AddBookGuardService implements CanDeactivate<AddBookComponent> {
    canDeactivate(component: AddBookComponent): boolean {
        if (component.addBookForm.dirty && !component.addBookForm.submitted) {
            return confirm('Are you sure you want to leave ?');
        }
        return true;
    }
}