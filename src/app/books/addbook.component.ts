import { Component, OnInit, ViewChild } from '@angular/core';
import{BooksService} from './books.service';
import{Book} from './book';
import{Router} from '@angular/router'

import bsCustomFileInput from 'bs-custom-file-input'
import { NgForm } from '@angular/forms';

@Component({
    selector: 'books-addbook',
    templateUrl: './addbook.component.html',
    styleUrls: ['./addbook.component.css']
  })
  export class AddBookComponent implements OnInit {
    @ViewChild('addForm', {static: false}) addBookForm:NgForm;
    constructor(private _booksService:BooksService, private router:Router) {        
    
    }
      
    ngOnInit() {
    
    }
    // selectedFile:File  = null;
    // imagePath="../../assets/images/";
    // onFileSelected(event){
    //   this.selectedFile = <File>event.target.files[0];
      
    //   this.image=this.selectedFile.name;
    //   this.imagePath += this.image;
    
    // }
    image:string="";
    addbook(formValue:any){
      console.log(formValue);
      const newbook:Book={
        title:formValue.title,
        subtitle:formValue.subtitle,
        description:formValue.description,
        author:formValue.author,
        category:formValue.category,
        price:formValue.price,
        quantity:formValue.quantity,
        // image:this.imagePath
      }
      this._booksService.addbook(newbook).subscribe(
      (book:any)=>{
        this.router.navigate(['/books']);

      })

    }

}