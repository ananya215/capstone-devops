import { Component, OnInit } from '@angular/core';
import{Book} from './book';
import{BooksService} from './books.service'


import{Router,RouterModule} from '@angular/router'
import { User } from '../users/User';
import { AuthService } from '../auth.service';
import { RecentViews } from './recent-views.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  
  constructor(private _recentViews:RecentViews,private _authService: AuthService,private _booksservice:BooksService) { 
    setInterval(() => {
      this.time = new Date().toLocaleTimeString();
    }, 1);

    setInterval(() => {
      this.day = new Date().toDateString();
    }, 1);

    this.views=this._recentViews.getviews();

  }

  ngOnInit() {
    console.log("In books.component.ts ")
    this.getBooks();
    //this.views=[this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
  
     //console.log(this.views);
    // this.getViews(this.books);
  
    // console.log(this.views);
    
  }
  //paath:string="../../assets/images/3.jpg";
  public time: any = new Date();
  public day: any = new Date();

  books:Book[];
  
  views:string[]=[];
  searchTerm:string;

  recents:string[];

  getBooks() {
    this._booksservice.getbooks().subscribe(
      (books:any) =>  {this.books = books
        //this.views=[this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title],
        //console.log(this.views);
      err => console.log(err)
       } );

   
 
  }


  search(formvalue){
    this.searchTerm = formvalue.searchTerm;
    //console.log(this.searchTerm)
  }


  


}
