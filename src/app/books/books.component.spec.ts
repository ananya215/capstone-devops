import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import{SearchFilterPipe} from './search-filter.pipe';
import { BooksComponent } from './books.component';
import { RecentViews } from './recent-views.service';
import { BooksService } from './books.service';
import { of } from 'rxjs';
import { AuthService } from '../auth.service';

describe('BooksComponent', () => {
  let component: BooksComponent;
  let fixture: ComponentFixture<BooksComponent>;
 let recentViewsStub: Partial<RecentViews>;
 let booksServiceStub: Partial<BooksService>;
 let authServiceStub: Partial<AuthService>;
 let recentViews;
 let bookService;
 let authService;

  beforeEach(async(() => {
    
    recentViewsStub={
      getviews: function(){return ["ABC","DEF","GHI"]}
    };

    booksServiceStub={
      getbooks(){
        const arr = ["kkk","jjj","iii"]
        return of(arr);
      }
    }



    TestBed.configureTestingModule({
      declarations: [ BooksComponent, SearchFilterPipe],
      providers:[
        {provide:RecentViews, useValue:recentViewsStub },
        {provide:BooksService, useValue:booksServiceStub },
        {provide:AuthService, useValue: authServiceStub}
       
      ],
      imports:[FormsModule,
        RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {

   // console.log(recentViews+"77")
    fixture = TestBed.createComponent(BooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    
  });
 
  
  it(`should contain recent Views`, ()=>{
   
    recentViews = fixture.debugElement.injector.get(RecentViews);  /* Injector for Service */ 
    //console.log(recentViews.getviews())
    expect(recentViews.getviews()).toContain('ABC')
  })

  it(`should have current time`,()=>{
   let time = new Date();
   
   expect((component.time).toLocaleTimeString()).toEqual(time.toLocaleTimeString())
  })

  it(`should have current date`,()=>{
    
    let day = new Date();
    expect((component.day).toDateString()).toEqual(day.toDateString())
  })

  it(`should have all the books`,()=>{
    bookService = fixture.debugElement.injector.get(BooksService);  /* Injector for Service */ 
    const result = bookService.getbooks()
    result.subscribe(val=> expect(val.length).toEqual(3))
   
  
  })

  

});
