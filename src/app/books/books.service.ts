import{Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import{Book} from './book'
import { Observable } from 'rxjs';

@Injectable()
export class BooksService{
    private _booksUrl="http://localhost:3200/books"
   
    private httpOptions={
        headers: new HttpHeaders({
            'Content-Type' : 'application/json'
        })
    };
   

    constructor(private _http:HttpClient){ }
    
    getbooks(){
        return this._http.get(this._booksUrl);
    }

    addbook(newBook:Book) : Observable<Book>{
        console.log(newBook);
        return this._http.post<Book>(this._booksUrl, newBook, this.httpOptions)
    }

    viewbook(id){
        //console.log(id)
        let viewbookURL = `${this._booksUrl}/${id}`;
        return this._http.get(viewbookURL);
    }
    
    updatebook(updatedBook,id){
        let viewbookURL = `${this._booksUrl}/${id}`;
        return this._http.put(viewbookURL, updatedBook, this.httpOptions)
    }
    deletebook(bookid){
        let deletebookURL = `${this._booksUrl}/${bookid}`;
        return this._http.delete(deletebookURL);
    }
}
