import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books.component';
import { AddBookComponent } from './addbook.component';

import{BookDetailGuardService} from './book-detail-guard.service'
import { ViewbookComponent } from './viewbook/viewbook.component';
import { UpdatebookComponent } from './updatebook/updatebook.component';
import { AddBookGuardService } from './addbook-guard-service';

const bookRoutes: Routes = [
  { path: '', component: BooksComponent },
  {path:'add', component:AddBookComponent,
  canActivate: [BookDetailGuardService],
  canDeactivate: [AddBookGuardService]},
  {path:':id', component:ViewbookComponent,
  canActivate: [BookDetailGuardService]},
  {path:'update/:id',component:UpdatebookComponent,
  canActivate: [BookDetailGuardService]}

 
];

@NgModule({
  imports: [
    RouterModule.forChild(bookRoutes)
  ],
  exports: [ RouterModule ]
})

export class BooksRoutingModule { }
