import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }   from '@angular/forms';
import { UpdatebookComponent } from './updatebook.component';
import { RouterModule } from '@angular/router';
import { BooksService } from '../books.service';
import { of } from 'rxjs';

describe('UpdatebookComponent', () => {
  let component: UpdatebookComponent;
  let fixture: ComponentFixture<UpdatebookComponent>;
  let booksServiceStub: Partial<BooksService>;
  let bookService;

  beforeEach(async(() => {
    booksServiceStub={
      
      deletebook(id){
        const arr1 ={3:"Rachanawali", 2:"Sanrachana"}
       // const arr = ["kkk","jjj","iii"]
    
      // 
      return id? of(delete arr1[id]):of("undefined")
      },
      viewbook(id){
        const arr1 ={3:"fdgdzty",4:"Rachana"}
     
        return id ? of(arr1[id]) : of("undefinedID")  ;
      },
      updatebook(name, id){
        
        const arr2 ={3:"Rachanawali"};
        arr2[id] = name;
        return id ? of(arr2[id]) : of("undefined");
      }
    }

    TestBed.configureTestingModule({
      declarations: [ UpdatebookComponent ],
      imports:[FormsModule,RouterModule.forRoot([])],
      providers:[
       
        {provide:BooksService, useValue:booksServiceStub }
      
       
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should return updated value`,()=>{
    bookService = fixture.debugElement.injector.get(BooksService);  /* Injector for Service */ 
    const result = bookService.updatebook("Sarswatichandran",3);
    result.subscribe(val=>expect(val).toEqual('Sarswatichandran'))

  })

  it(`should delete the given ID`,()=>{
    bookService = fixture.debugElement.injector.get(BooksService); 
    const result = bookService.deletebook(3);
    result.subscribe(val=>expect(val).toBeTruthy())
   
  })
}); 



