import { Component, OnInit } from '@angular/core';
import{ActivatedRoute,Params,Router} from '@angular/router';
import { BooksService } from '../books.service';
import { Book } from '../book';


@Component({
  selector: 'app-updatebook',
  templateUrl: './updatebook.component.html',
  styleUrls: ['./updatebook.component.css']
})
export class UpdatebookComponent implements OnInit {
  id:number;
  book:any={};
  constructor(private _booksservice:BooksService,private route: ActivatedRoute,private router:Router) { }
 
  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id = +params['id'];
      //console.log(params)
  });
  this.updateBook(this.id);
  }

  
  updateBook(id){
    this._booksservice.viewbook(id).subscribe(
      (data:any) =>{ this.book=data;
        //console.log(this.book)
      }
    );
  }

  saveChanges(formValue){
    const updatedBook:Book = formValue;

    this._booksservice.updatebook(updatedBook,this.id).subscribe(
      (data:any) => this.router.navigate(['books'])
      );
  }
  deleteBook(bookid){
    //console.log(bookid)

    this._booksservice.deletebook(bookid).subscribe(
    (data:any)=> {
      //console.log(data)
      this.router.navigate(['books'])
    }
    );
   }
}
