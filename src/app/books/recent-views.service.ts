import{Injectable} from '@angular/core';
import{BooksService} from './books.service';
import {Book} from './book';

@Injectable()
export class RecentViews{
    recentView:string[]=[];
    books:Book[]=[];
    constructor(private _booksService:BooksService){ 
      
        this.getviewsbybooks();
       // console.log(this.books)
     //this.recentView= [this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
    }

    getviews(){
      console.log(this.recentView);
      return this.recentView;
    }
    
    setviews(name){
      console.log(this.recentView)
        if(this.recentView.length<5)
        {
          if(this.recentView.indexOf(name) !== -1 ){
              console.log(" EXISTS"+name)
          } else{
            this.recentView.push(name);
          }
    
        }else{
          this.recentView.shift();
          if(this.recentView.indexOf(name) !== -1 ){
              console.log("EXISTS"+name)
          } else{
            this.recentView.push(name);
          }
        }
    }
    getviewsbybooks(){

      this._booksService.getbooks().subscribe(
        (books:any) => { this.books = books,
          this.recentView= [this.books[0].title, this.books[1].title, this.books[2].title, this.books[3].title, this.books[4].title];
          console.log(this.recentView)
          err => console.log(err)
        })
        //console.log(this.recentView)
             
    }

    
}