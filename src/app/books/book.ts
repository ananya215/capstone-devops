export class Book{
    title:string;
    subtitle:string;
    description:string;
    author:string;
    category:string;
    price:number;
    quantity:number;
    // image:string;

}