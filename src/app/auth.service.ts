import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  logout() {
     if(this.isLoggedIn())
    localStorage.removeItem('currentUser');
    else{
      console.log(localStorage)
    }
  }
  isLoggedIn() {
    if (localStorage.getItem('currentUser') != null)
      return true;
    else
      return false;
  }
}