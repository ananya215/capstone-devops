import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';
import{HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
// import { BooksComponent } from './books/books.component';
import { ContactComponent } from './contact/contact.component';
import { SigninComponent } from './signin/signin.component';
import { RegisterComponent } from './register/register.component';

//import{SearchFilterPipe} from './search-filter.pipe';

import{ConfirmEqualValidatorDirective} from './confirm-equal-validator.directive';

import{BooksModule} from './books/books.module'
import{UserService } from './users/user.service';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    PageNotFoundComponent,
    ContactComponent,
    SigninComponent,
    RegisterComponent,ConfirmEqualValidatorDirective, ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,HttpClientModule,ShowHidePasswordModule
    // NgbModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
 }
