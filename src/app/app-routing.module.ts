import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import{HomeComponent} from './home/home.component';
import{AboutComponent} from './about/about.component';
import{BooksComponent} from './books/books.component';
import{ContactComponent} from './contact/contact.component';
import{SigninComponent} from './signin/signin.component';
import{RegisterComponent} from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFoundComponent } from './page-not-found.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'about', component:AboutComponent},
  { path: 'books', loadChildren: () => import('./books/books.module').then(m => m.BooksModule) },
  {path:'contact', component:ContactComponent},
  {path:'signin', component:SigninComponent},
  {path:'register', component:RegisterComponent},
  {path:'profile', component:ProfileComponent},
  { path: '**', component: PageNotFoundComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
