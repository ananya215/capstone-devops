import{Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import{User} from './User';
import { Observable } from 'rxjs';

@Injectable()
export class UserService{
    private _userUrl="http://localhost:3000/users"
    private count=3;
    private httpOptions={
        headers: new HttpHeaders({
            'Content-Type' : 'application/json'
        })
    };
   

    constructor(private _http:HttpClient){ }
    
    getUsers(){
        return this._http.get(this._userUrl);
    }

    addUser(newUser:User) : Observable<User>{
        console.log(newUser);
        return this._http.post<User>(this._userUrl, newUser, this.httpOptions)
    }
}
