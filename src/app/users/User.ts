export class User{
    fname:string;
    lname:string;
    email:string;
    password:string;
    contact:number;
    location:string;
}