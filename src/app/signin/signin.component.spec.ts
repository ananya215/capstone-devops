import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { SigninComponent } from './signin.component';

import { AuthService } from '../auth.service';
import {UserService} from '../users/user.service'
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
  let authService;
  let userService;
  let authServiceStub: Partial<AuthService>;
  let userServiceStub : Partial<UserService>;

  beforeEach(async(() => {
    userServiceStub={
      getUsers(){
       const arr= ["Raju","Pinki","Shailesh"];
        return of(arr);
      }
    }

    TestBed.configureTestingModule({
      declarations: [ SigninComponent ],
      imports:[FormsModule,RouterTestingModule],
      providers:[
        {provide:AuthService, useValue: authServiceStub},
        {provide :UserService, useValue : userServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create signIn Component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a routerlink for signUp`,()=>{
    let href = fixture.debugElement.query(By.css('.bottom-text-w3ls')).nativeElement
    .getAttribute('routerLink')
    expect(href).toEqual('/register')
  })

  // it(`should have email`,()=>{
  //   let loginEl = fixture.debugElement.query(By.css('input[type=email]'));
  //   console.log(loginEl)
    
  // })
});
