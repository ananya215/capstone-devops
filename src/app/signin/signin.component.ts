import { Component, OnInit } from '@angular/core';
import{UserService} from '../users/user.service';
import{User} from '../users/User';

import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  allusers: User[];
  
  constructor(private _userService:UserService, private router:Router, private _authService : AuthService) { }

  ngOnInit() {
    this.getUsers();
   
  }
  
  getUsers(){
    this._userService.getUsers().subscribe(
    (users:any)=> this.allusers=users,
    err=>console.log(err)
    );
  }


  signin(formValue:any){
    
    
    console.log(formValue)
    for(let i = 0; i<this.allusers.length; i++){ 
      
    
      if(this.allusers[i].email==formValue.username){
        
        if(this.allusers[i].password != formValue.pwd){
          console.log("pwd not matched")
          alert("Password doesn't match")
          break;
        }
        console.log(this.allusers[i].email)
        let usr = this.allusers[i];
        localStorage.setItem("currentUser", JSON.stringify(usr));
       console.log(localStorage)
        break;
      }
      
    }
    if(!this._authService.isLoggedIn()){
      alert("Please Try to login with valid credentials. If you're not registered then sign up")
    }
    else{
     // console.log(localStorage.getItem('currentUser'))
       
       
       this.router.navigate(['/']);
      
      
     
    }
    
    

  }
}
