import { Component,OnInit } from '@angular/core';
import{Router} from '@angular/router';
import { AuthService } from './auth.service';
import { Title }     from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BookZone';
  c:object;
  flag:boolean= false;
  
  

  constructor(private router : Router,private _authService : AuthService,private titleService: Title){
    this.titleService.setTitle( this.title ); /* titleService sets title in the tab */
  // console.log(this.flag);
  // console.log("In COnstructor")
  //  console.log(localStorage);
    if(this._authService.isLoggedIn()){
      this.c= JSON.parse(localStorage.getItem('currentUser'))
      this.flag=true;
   //   console.log(this.c);
      
    }
   // console.log(this.flag)
  }

  ngOnInit() {
    this.router.navigate([''])

  }

 

  logout(){
    this._authService.logout();
    localStorage.removeItem('currentUser')
    location.reload();
  }
  
  
  
}
