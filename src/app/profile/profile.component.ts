import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../users/User';
import { Location } from "@angular/common";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private _authService: AuthService,private location: Location) { }

  ngOnInit() {
    if(this._authService.isLoggedIn){
      this.user=JSON.parse(localStorage.getItem('currentUser'))
      this.fullname=this.user.fname +" "+ this.user.lname;
    }
  }
  user:User;
  fullname:string;

  goBack(): void {
    this.location.back();
  }
   
}
