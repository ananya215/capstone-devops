import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthService } from './auth.service';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'BookZone'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('BookZone');
  });

  // it('#loggedIN should return false', () => {
  //   // const fixture = TestBed.createComponent(AppComponent);
  //   // fixture.detectChanges();
  //   // const compiled = fixture.debugElement.nativeElement;
  //   // expect(compiled.querySelector('.ff').textContent).toContain('BookZone');

  //   const service = new AuthService();
  //   const result = service.isLoggedIn();
  //   expect(result).toBeFalsy();
  // });

  it(`should active Home link`, ()=>{
    const fixture = TestBed.createComponent(AppComponent);
    let href = fixture.debugElement.query(By.css('a')).nativeElement
    .getAttribute('routerLinkActive');
    //console.log( fixture.debugElement.query(By.css('a')))
   expect(href).toEqual('active');
  })

});
