(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "./$$_lazy_route_resource lazy recursive":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function $$_lazy_route_resourceLazyRecursive(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
      /***/
    },

    /***/
    "./src/app/about/about.component.ts":
    /*!******************************************!*\
      !*** ./src/app/about/about.component.ts ***!
      \******************************************/

    /*! exports provided: AboutComponent */

    /***/
    function srcAppAboutAboutComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AboutComponent", function () {
        return AboutComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AboutComponent = /*#__PURE__*/function () {
        function AboutComponent() {
          _classCallCheck(this, AboutComponent);
        }

        _createClass(AboutComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AboutComponent;
      }();

      AboutComponent.ɵfac = function AboutComponent_Factory(t) {
        return new (t || AboutComponent)();
      };

      AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AboutComponent,
        selectors: [["app-about"]],
        decls: 47,
        vars: 0,
        consts: [[1, "mainBg"], [1, "addbookform"], [1, "title"], [1, "about-para"], [1, "partners"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"]],
        template: function AboutComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " About Us ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " In the fall of 2013, the 25-year-old Vincent Lou left SYstemZone and started this business.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " His passion for products and frustration with the lack of transparency in pricing led them to build Club Factory.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " While the team skews young, our employees have cut their teeth at places like Amazon, Facebook, Gap, Zara, and J.Crew. We share the passion for pushing boundaries and challenging conventions.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " If you are a rule breaker, questioner, student who skip classes, join us! Contact us at hr@clubfactory.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "BookZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "i", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "i", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "i", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, " Box 564, Disneyland");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " Bangalore ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Visit us at");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, " BookZone Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 100vh;\n    overflow:hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:100px;\n    margin-left:17%;\n    position: absolute;\n    height: 70%;\n    width: 70%;\n    scroll-behavior: auto;\n    \n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n}\n.about-para[_ngcontent-%COMP%]{\n    padding: 12px;\n    margin:19px;\n    border:5px solid black;\n}\n.partners[_ngcontent-%COMP%]{\n    margin-top:13%;\n    background-color: #A0DEE2;\n    padding: 12px;\n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    height: 100px;\n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:65%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\naddress[_ngcontent-%COMP%]{\n    float: right;\n    font-size: 12px;\n    margin-top:-7%;\n    margin-right:30px;\n    text-align: left;\n}\n.mailus[_ngcontent-%COMP%]{\n  font-size: 12px;\n  margin-top:-2%;\n padding: 12px;\n  margin-left:63%;\n  text-align: left;\n}\n.vl[_ngcontent-%COMP%] {\n  border-left: 1px solid black;\n  height: 83px;\n  margin-right:10%;\n  margin-left:-6%;\n  position: absolute;\n  margin-top:0px;\n}\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRDQUE4RDtJQUM5RCxzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFVBQVU7SUFDVixxQkFBcUI7O0FBRXpCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsNkJBQTZCO0lBQzdCLDZDQUE2QztJQUM3QyxxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSxhQUFhO0lBQ2IsV0FBVztJQUNYLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksY0FBYztJQUNkLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsNkNBQTZDO0lBQzdDLGFBQWE7QUFDakI7QUFHQTs7SUFFSSxjQUFjOztLQUViLGNBQWM7S0FDZCxpQkFBaUI7S0FDakIsYUFBYTtLQUNiLFNBQVM7S0FDVCxVQUFVO0lBQ1gsa0JBQWtCO0tBQ2pCLHVCQUF1QjtRQUNwQixrQkFBa0I7S0FDckIsa0JBQWtCO0tBQ2xCLGlCQUFpQjtHQUNuQjtBQUNBO0lBQ0MsWUFBWTtJQUNaLGVBQWU7SUFDZixjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLGdCQUFnQjtBQUNwQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLGNBQWM7Q0FDZixhQUFhO0VBQ1osZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsNEJBQTRCO0VBQzVCLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixjQUFjO0FBQ2hCO0FBQ0c7T0FDSSxXQUFXO09BQ1gsZUFBZTtPQUNmLGdCQUFnQjtHQUNwQjtBQUNGO0tBQ0ksV0FBVztLQUNYLGVBQWU7O0tBRWYsY0FBYztDQUNsQjtBQUNBO0tBQ0ksZ0JBQWdCO0NBQ3BCO0FBQ0E7S0FDSSxjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxXQUFXO0tBQ1gsZUFBZTtLQUNmLGlCQUFpQjs7Q0FFckI7QUFDQTtLQUNJLGFBQWE7S0FDYjtDQUNKIiwiZmlsZSI6InNyYy9hcHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluQmd7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvYm9vay5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG4gICAgb3ZlcmZsb3c6aGlkZGVuO1xufVxuLmFkZGJvb2tmb3Jte1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6MTAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6MTclO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDcwJTtcbiAgICB3aWR0aDogNzAlO1xuICAgIHNjcm9sbC1iZWhhdmlvcjogYXV0bztcbiAgICBcbn1cbi50aXRsZXtcbiAgICBmb250LXNpemU6MzBweDtcbiAgICAvKiBib3JkZXI6IDFweCBzb2xpZCBibGFjazsgKi9cbiAgICBib3gtc2hhZG93OiAwIDE1cHggMjVweCAtMTBweCByZ2JhKDAsMCwwLC4zNSk7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQTBERUUyO1xufVxuLmFib3V0LXBhcmF7XG4gICAgcGFkZGluZzogMTJweDtcbiAgICBtYXJnaW46MTlweDtcbiAgICBib3JkZXI6NXB4IHNvbGlkIGJsYWNrO1xufVxuLnBhcnRuZXJze1xuICAgIG1hcmdpbi10b3A6MTMlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNBMERFRTI7XG4gICAgcGFkZGluZzogMTJweDtcbiAgICBib3gtc2hhZG93OiAwIDE1cHggMjVweCAtMTBweCByZ2JhKDAsMCwwLC4zNSk7XG4gICAgaGVpZ2h0OiAxMDBweDtcbn1cblxuXG4uZm9vdGVyIHtcbiBcbiAgICBtYXJnaW4tbGVmdDo2JTtcbiAgICBcbiAgICAgbWFyZ2luLXRvcDo2NSU7XG4gICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICBib3R0b206IDA7XG4gICAgIHdpZHRoOiA4OSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGJvcmRlci1yYWRpdXM6NXB4O1xuICAgfVxuICAgYWRkcmVzc3tcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbi10b3A6LTclO1xuICAgIG1hcmdpbi1yaWdodDozMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ubWFpbHVze1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6LTIlO1xuIHBhZGRpbmc6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OjYzJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi52bCB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgYmxhY2s7XG4gIGhlaWdodDogODNweDtcbiAgbWFyZ2luLXJpZ2h0OjEwJTtcbiAgbWFyZ2luLWxlZnQ6LTYlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6MHB4O1xufVxuICAgLmZvb3Rlci1pY29ue1xuICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgIG1hcmdpbi10b3A6MTVweDtcbiAgICAgICBtYXJnaW4tbGVmdDoxNXB4O1xuICAgfVxuIC5sb2dvLWZvb3RlcntcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIG1hcmdpbi10b3A6MjFweDtcbiAgICAgXG4gICAgIGZvbnQtc2l6ZToyMHB4O1xuIH1cbiAuZmItaWNvbntcbiAgICAgbWFyZ2luLWxlZnQ6LTEwJTtcbiB9XG4gI3NvY2lhbC1mYjpob3ZlciB7XG4gICAgIGNvbG9yOiAjM0I1OTk4O1xuIH1cbiAjc29jaWFsLXR3OmhvdmVyIHtcbiAgICAgY29sb3I6ICM0MDk5RkY7XG4gfVxuICNzb2NpYWwtZ3A6aG92ZXIge1xuICAgICBjb2xvcjogI2QzNDgzNjtcbiB9XG4gI3NvY2lhbC1lbTpob3ZlciB7XG4gICAgIGNvbG9yOiAjZjM5YzEyO1xuIH1cbiAuc29jaWFsLWljb25ze1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICBtYXJnaW4tbGVmdDoxNzBweDtcbiAgICAgXG4gfVxuIC5zb2NpYWwtaWNvbnMgYXtcbiAgICAgcGFkZGluZzogMTBweDtcbiAgICAgY29sb3I6cmdiKDkwLCAyMDEsIDIzOClcbiB9Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-about',
            templateUrl: './about.component.html',
            styleUrls: ['./about.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/app-routing.module.ts":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./home/home.component */
      "./src/app/home/home.component.ts");
      /* harmony import */


      var _about_about_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./about/about.component */
      "./src/app/about/about.component.ts");
      /* harmony import */


      var _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./contact/contact.component */
      "./src/app/contact/contact.component.ts");
      /* harmony import */


      var _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./signin/signin.component */
      "./src/app/signin/signin.component.ts");
      /* harmony import */


      var _register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register/register.component */
      "./src/app/register/register.component.ts");
      /* harmony import */


      var _profile_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./profile/profile.component */
      "./src/app/profile/profile.component.ts");
      /* harmony import */


      var _page_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./page-not-found.component */
      "./src/app/page-not-found.component.ts");

      var routes = [{
        path: '',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"]
      }, {
        path: 'about',
        component: _about_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]
      }, {
        path: 'books',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | books-books-module */
          "books-books-module").then(__webpack_require__.bind(null,
          /*! ./books/books.module */
          "./src/app/books/books.module.ts")).then(function (m) {
            return m.BooksModule;
          });
        }
      }, {
        path: 'contact',
        component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"]
      }, {
        path: 'signin',
        component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__["SigninComponent"]
      }, {
        path: 'register',
        component: _register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"]
      }, {
        path: 'profile',
        component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_7__["ProfileComponent"]
      }, {
        path: '**',
        component: _page_not_found_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponent"]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/app.component.ts":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      function AppComponent_span_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Hii ", ctx_r0.c.fname, " !");
        }
      }

      function AppComponent_li_14_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "SIGNIN");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AppComponent_a_17_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "PROFILE");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0() {
        return {
          exact: true
        };
      };

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(router, _authService, titleService) {
          _classCallCheck(this, AppComponent);

          this.router = router;
          this._authService = _authService;
          this.titleService = titleService;
          this.title = 'BookZone';
          this.flag = false;
          this.titleService.setTitle(this.title);
          /* titleService sets title in the tab */
          // console.log(this.flag);
          // console.log("In COnstructor")
          //  console.log(localStorage);

          if (this._authService.isLoggedIn()) {
            this.c = JSON.parse(localStorage.getItem('currentUser'));
            this.flag = true; //   console.log(this.c);
          } // console.log(this.flag)

        }

        _createClass(AppComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.router.navigate(['']);
          }
        }, {
          key: "logout",
          value: function logout() {
            this._authService.logout();

            localStorage.removeItem('currentUser');
            location.reload();
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"]));
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 28,
        vars: 5,
        consts: [[1, "nav_w3ls", "navigation"], [1, "fas", "fa-book", "fa-3x"], ["class", "usr", 4, "ngIf"], [1, "ff"], ["routerLink", "", "routerLinkActive", "active", 3, "routerLinkActiveOptions"], ["routerLink", "/about", "routerLinkActive", "active"], ["routerLink", "/books", "routerLinkActive", "active"], [4, "ngIf"], [1, "dropdown"], ["class", "dropbtn", "routerLink", "/profile", "routerLinkActive", "active", 4, "ngIf"], [1, "dropdown-content"], ["routerLink", "/profile", "routerLinkActive", "active"], [1, "dropbtn", 3, "click"], [1, "fas", "fa-power-off"], ["routerLink", "/contact", "routerLinkActive", "active"], [1, "usr"], ["routerLink", "/signin", "routerLinkActive", "active"], ["routerLink", "/profile", "routerLinkActive", "active", 1, "dropbtn"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, AppComponent_span_2_Template, 2, 1, "span", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "nav", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ul");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "HOME");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "ABOUT");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "BOOKS");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, AppComponent_li_14_Template, 3, 0, "li", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, AppComponent_a_17_Template, 2, 0, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "View details");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_21_listener() {
              return ctx.logout();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Logout");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "CONTACT");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "router-outlet");
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.flag);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLinkActiveOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.flag);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.flag);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: [".usr[_ngcontent-%COMP%]{\n    margin-left:2%;\n    display: inline-block;\n   \n   font-size: 22px;\n   \n   color:black;\n}\n.navigation[_ngcontent-%COMP%]{\n    position: relative;\n    max-width: 1200px;\n    margin: 20px auto;\n    padding: 10px;\n    background: white;\n    box-sizing: border-box;\n    border-radius: 4px;\n    box-shadow: 0 2px 5px rgba(0,0,0,.2);\n}\n.ff[_ngcontent-%COMP%]{\n    float:right;\n\n}\n.nav_w3ls[_ngcontent-%COMP%] {\n    position: absolute;\n    left: 0;\n    right: 0;\n    z-index: 99;\n}\n.clearfix[_ngcontent-%COMP%]{\n    clear:both;\n}\n.ff[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{\n    margin:0;\n    padding: 0;\n    display: flex;\n}\n.ff[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{\n    list-style: none;\n   \n}\n.ff[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    display: block;\n    margin:2px 0;\n    padding:10px 20px;\n    text-decoration: none;\n    color:#262626;\n}\n.ff[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.active[_ngcontent-%COMP%], .ff[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover{\n    background:black;\n    color:#fff;\n    transition:0.5s;\n}\n@media (max-width:1200px){\n    .navigation[_ngcontent-%COMP%]{\n        margin:20px;\n    }\n}\n.dropbtn[_ngcontent-%COMP%] {\n  \n     color: white;\n    padding: 16px;\n    font-size: 16px;\n    border: none;\n  }\n.dropbtn[_ngcontent-%COMP%]:hover{\n       color: black\n   }\n.dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n  }\n.dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #f1f1f1;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: black;\n    padding: 12px 16px;\n    text-decoration: none;\n    display: block;\n  }\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: black;}\n.dropdown-content[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover{background-color: #008CBA;}\n.dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n.dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #f1f1f1;}\n.dropbtn[_ngcontent-%COMP%]{\n    color: black\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjs7R0FFdEIsZUFBZTtHQUNmLDRCQUE0QjtHQUM1QixXQUFXO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixvQ0FBb0M7QUFDeEM7QUFDQTtJQUNJLFdBQVc7O0FBRWY7QUFHQTtJQUNJLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsUUFBUTtJQUNSLFdBQVc7QUFDZjtBQUVBO0lBQ0ksVUFBVTtBQUNkO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsVUFBVTtJQUNWLGFBQWE7QUFDakI7QUFDQTtJQUNJLGdCQUFnQjs7QUFFcEI7QUFDQTtJQUNJLGNBQWM7SUFDZCxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHFCQUFxQjtJQUNyQixhQUFhO0FBQ2pCO0FBQ0M7O0lBRUcsZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixlQUFlO0FBQ25CO0FBQ0E7SUFDSTtRQUNJLFdBQVc7SUFDZjtBQUNKO0FBRUM7O0tBRUksWUFBWTtJQUNiLGFBQWE7SUFDYixlQUFlO0lBQ2YsWUFBWTtFQUNkO0FBQ0M7T0FDSTtHQUNKO0FBQ0Q7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBRUE7SUFDRSxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixnQkFBZ0I7SUFDaEIsNENBQTRDO0lBQzVDLFVBQVU7RUFDWjtBQUVBO0lBQ0UsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsY0FBYztFQUNoQjtBQUVBLDJCQUEyQix1QkFBdUIsQ0FBQztBQUVuRCwrQkFBK0IseUJBQXlCLENBQUM7QUFFekQsbUNBQW1DLGNBQWMsQ0FBQztBQUVsRCwwQkFBMEIseUJBQXlCLENBQUM7QUFFdEQ7SUFDSTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi51c3J7XG4gICAgbWFyZ2luLWxlZnQ6MiU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgXG4gICBmb250LXNpemU6IDIycHg7XG4gICAvKiBiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrOyAqL1xuICAgY29sb3I6YmxhY2s7XG59XG4ubmF2aWdhdGlvbntcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWF4LXdpZHRoOiAxMjAwcHg7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA1cHggcmdiYSgwLDAsMCwuMik7XG59XG4uZmZ7XG4gICAgZmxvYXQ6cmlnaHQ7XG5cbn1cblxuXG4ubmF2X3czbHMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIHotaW5kZXg6IDk5O1xufVxuXG4uY2xlYXJmaXh7XG4gICAgY2xlYXI6Ym90aDtcbn1cbi5mZiAgdWx7XG4gICAgbWFyZ2luOjA7XG4gICAgcGFkZGluZzogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuLmZmICB1bCBsaXtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgXG59XG4uZmYgIHVsIGxpIGF7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOjJweCAwO1xuICAgIHBhZGRpbmc6MTBweCAyMHB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjojMjYyNjI2O1xufVxuIC5mZiB1bCBsaSBhLmFjdGl2ZSxcbi5mZiB1bCBsaSBhOmhvdmVye1xuICAgIGJhY2tncm91bmQ6YmxhY2s7XG4gICAgY29sb3I6I2ZmZjtcbiAgICB0cmFuc2l0aW9uOjAuNXM7XG59XG5AbWVkaWEgKG1heC13aWR0aDoxMjAwcHgpe1xuICAgIC5uYXZpZ2F0aW9ue1xuICAgICAgICBtYXJnaW46MjBweDtcbiAgICB9XG59XG5cbiAuZHJvcGJ0biB7XG4gIFxuICAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMTZweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgYm9yZGVyOiBub25lO1xuICB9IFxuICAgLmRyb3BidG46aG92ZXJ7XG4gICAgICAgY29sb3I6IGJsYWNrXG4gICB9XG4gIC5kcm9wZG93biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxuICBcbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XG4gICAgbWluLXdpZHRoOiAxNjBweDtcbiAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcbiAgICB6LWluZGV4OiAxO1xuICB9XG4gIFxuICAuZHJvcGRvd24tY29udGVudCBhIHtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgcGFkZGluZzogMTJweCAxNnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICBcbiAgLmRyb3Bkb3duLWNvbnRlbnQgYTpob3ZlciB7YmFja2dyb3VuZC1jb2xvcjogYmxhY2s7fVxuXG4gIC5kcm9wZG93bi1jb250ZW50IGJ1dHRvbjpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiAjMDA4Q0JBO31cbiAgXG4gIC5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tY29udGVudCB7ZGlzcGxheTogYmxvY2s7fVxuICBcbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWYxO31cblxuLmRyb3BidG57XG4gICAgY29sb3I6IGJsYWNrXG59XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
          }, {
            type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/app.module.ts":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./app-routing.module */
      "./src/app/app-routing.module.ts");
      /* harmony import */


      var ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ngx-show-hide-password */
      "./node_modules/ngx-show-hide-password/__ivy_ngcc__/fesm2015/ngx-show-hide-password.js");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./app.component */
      "./src/app/app.component.ts");
      /* harmony import */


      var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./home/home.component */
      "./src/app/home/home.component.ts");
      /* harmony import */


      var _about_about_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./about/about.component */
      "./src/app/about/about.component.ts");
      /* harmony import */


      var _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./contact/contact.component */
      "./src/app/contact/contact.component.ts");
      /* harmony import */


      var _signin_signin_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./signin/signin.component */
      "./src/app/signin/signin.component.ts");
      /* harmony import */


      var _register_register_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./register/register.component */
      "./src/app/register/register.component.ts");
      /* harmony import */


      var _confirm_equal_validator_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./confirm-equal-validator.directive */
      "./src/app/confirm-equal-validator.directive.ts");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ./profile/profile.component */
      "./src/app/profile/profile.component.ts");
      /* harmony import */


      var _page_not_found_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./page-not-found.component */
      "./src/app/page-not-found.component.ts"); // import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
      // import { BooksComponent } from './books/books.component';
      //import{SearchFilterPipe} from './search-filter.pipe';


      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [_users_user_service__WEBPACK_IMPORTED_MODULE_13__["UserService"]],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_5__["ShowHidePasswordModule"] // NgbModule
        ]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_8__["AboutComponent"], _page_not_found_component__WEBPACK_IMPORTED_MODULE_15__["PageNotFoundComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__["ContactComponent"], _signin_signin_component__WEBPACK_IMPORTED_MODULE_10__["SigninComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_11__["RegisterComponent"], _confirm_equal_validator_directive__WEBPACK_IMPORTED_MODULE_12__["ConfirmEqualValidatorDirective"], _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__["ProfileComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_5__["ShowHidePasswordModule"] // NgbModule
          ]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_8__["AboutComponent"], _page_not_found_component__WEBPACK_IMPORTED_MODULE_15__["PageNotFoundComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_9__["ContactComponent"], _signin_signin_component__WEBPACK_IMPORTED_MODULE_10__["SigninComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_11__["RegisterComponent"], _confirm_equal_validator_directive__WEBPACK_IMPORTED_MODULE_12__["ConfirmEqualValidatorDirective"], _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__["ProfileComponent"]],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_5__["ShowHidePasswordModule"] // NgbModule
            ],
            providers: [_users_user_service__WEBPACK_IMPORTED_MODULE_13__["UserService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/auth.service.ts":
    /*!*********************************!*\
      !*** ./src/app/auth.service.ts ***!
      \*********************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AuthService = /*#__PURE__*/function () {
        function AuthService() {
          _classCallCheck(this, AuthService);
        }

        _createClass(AuthService, [{
          key: "logout",
          value: function logout() {
            if (this.isLoggedIn()) localStorage.removeItem('currentUser');else {
              console.log(localStorage);
            }
          }
        }, {
          key: "isLoggedIn",
          value: function isLoggedIn() {
            if (localStorage.getItem('currentUser') != null) return true;else return false;
          }
        }]);

        return AuthService;
      }();

      AuthService.ɵfac = function AuthService_Factory(t) {
        return new (t || AuthService)();
      };

      AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: AuthService,
        factory: AuthService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/confirm-equal-validator.directive.ts":
    /*!******************************************************!*\
      !*** ./src/app/confirm-equal-validator.directive.ts ***!
      \******************************************************/

    /*! exports provided: ConfirmEqualValidatorDirective */

    /***/
    function srcAppConfirmEqualValidatorDirectiveTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfirmEqualValidatorDirective", function () {
        return ConfirmEqualValidatorDirective;
      });
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ConfirmEqualValidatorDirective = /*#__PURE__*/function () {
        function ConfirmEqualValidatorDirective() {
          _classCallCheck(this, ConfirmEqualValidatorDirective);
        }

        _createClass(ConfirmEqualValidatorDirective, [{
          key: "validate",
          value: function validate(control) {
            var controlToCompare = control.parent.get(this.appConfirmEqualValidator); // console.log( (control.parent.get(this.appConfirmEqualValidator)).value)
            // console.log(controlToCompare)
            // console.log(controlToCompare.value)

            if (controlToCompare && controlToCompare.value !== control.value) {
              console.log("jhdfgagfd");
              return {
                'notEqual': true
              };
            }

            return null;
          }
        }]);

        return ConfirmEqualValidatorDirective;
      }();

      ConfirmEqualValidatorDirective.ɵfac = function ConfirmEqualValidatorDirective_Factory(t) {
        return new (t || ConfirmEqualValidatorDirective)();
      };

      ConfirmEqualValidatorDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: ConfirmEqualValidatorDirective,
        selectors: [["", "appConfirmEqualValidator", ""]],
        inputs: {
          appConfirmEqualValidator: "appConfirmEqualValidator"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
          provide: _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NG_VALIDATORS"],
          useExisting: ConfirmEqualValidatorDirective,
          multi: true
        }])]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ConfirmEqualValidatorDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[appConfirmEqualValidator]',
            providers: [{
              provide: _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NG_VALIDATORS"],
              useExisting: ConfirmEqualValidatorDirective,
              multi: true
            }]
          }]
        }], null, {
          appConfirmEqualValidator: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "./src/app/contact/contact.component.ts":
    /*!**********************************************!*\
      !*** ./src/app/contact/contact.component.ts ***!
      \**********************************************/

    /*! exports provided: ContactComponent */

    /***/
    function srcAppContactContactComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactComponent", function () {
        return ContactComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ContactComponent = /*#__PURE__*/function () {
        function ContactComponent() {
          _classCallCheck(this, ContactComponent);
        }

        _createClass(ContactComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ContactComponent;
      }();

      ContactComponent.ɵfac = function ContactComponent_Factory(t) {
        return new (t || ContactComponent)();
      };

      ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ContactComponent,
        selectors: [["app-contact"]],
        decls: 66,
        vars: 0,
        consts: [[1, "mainBg"], [1, "addbookform"], [1, "fas", "fa-bookmark", "fa-4x", "fa-rotate-270", "bookIcon"], [1, "bookP"], [1, "fas", "fa-bookmark", "fa-4x", "fa-rotate-270", "bookIcon1"], [1, "bookP1"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"]],
        template: function ContactComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Our Address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Flipkart Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " Outer Ring Road, Devarabeesanahalli Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Bengaluru, 560103,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " Karnataka, India");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "i", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Mail:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " Box 564, Clove Embassy");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, " Bangalore ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "i", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "call us at:9897576628, 9987675567 ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "i", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "BookZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "i", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "a", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "i", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "i", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "i", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, " Box 564, Disneyland");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, " USA ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Visit us at");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, " Flipkart Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 130vh;\n    overflow: hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:100px;\n    margin-left:33%;\n    position: absolute;\n    height: 95%;\n    width: 35%;\n    scroll-behavior: auto;\n    \n}\n.bookIcon[_ngcontent-%COMP%]{\n    margin-top:5%; \n    color: rgb(101, 201, 235);\n\n}\n.bookP[_ngcontent-%COMP%]{\n    border: 1px solid ;\n    margin-left:8.5%;\n    margin-top:-10%;\n    padding-left:10px;\n    background-color:black ;\n    color: white;\n}\naddress[_ngcontent-%COMP%]{\n    padding: 20px;\n    \n}\n.bookIcon1[_ngcontent-%COMP%]{\n    margin-top:-2%; \n    color: rgb(101, 201, 235);\n\n}\n.bookP1[_ngcontent-%COMP%]{\n    border: 1px solid ;\n    margin-left:8.5%;\n    margin-top:-10%;\n    padding-left:10px;\n    background-color:black ;\n    color: white;\n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:65%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\n.footer[_ngcontent-%COMP%]   address[_ngcontent-%COMP%]{\n    float: right;\n    font-size: 12px;\n    margin-top:-9%;\n    margin-right:30px;\n    text-align: left;\n}\n.mailus[_ngcontent-%COMP%]{\n  font-size: 12px;\n  margin-top:-2%;\n padding: 12px;\n  margin-left:63%;\n  text-align: left;\n}\n.vl[_ngcontent-%COMP%] {\n  border-left: 1px solid black;\n  height: 83px;\n  margin-right:10%;\n  margin-left:-6%;\n  position: absolute;\n  margin-top:0px;\n}\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0Q0FBOEQ7SUFDOUQsc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QiwyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFVBQVU7SUFDVixxQkFBcUI7O0FBRXpCO0FBQ0E7SUFDSSxhQUFhO0lBQ2IseUJBQXlCOztBQUU3QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxhQUFhOztBQUVqQjtBQUNBO0lBQ0ksY0FBYztJQUNkLHlCQUF5Qjs7QUFFN0I7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsWUFBWTtBQUNoQjtBQUdBOztJQUVJLGNBQWM7O0tBRWIsY0FBYztLQUNkLGlCQUFpQjtLQUNqQixhQUFhO0tBQ2IsU0FBUztLQUNULFVBQVU7SUFDWCxrQkFBa0I7S0FDakIsdUJBQXVCO1FBQ3BCLGtCQUFrQjtLQUNyQixrQkFBa0I7S0FDbEIsaUJBQWlCO0dBQ25CO0FBQ0E7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztDQUNmLGFBQWE7RUFDWixlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSw0QkFBNEI7RUFDNUIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7QUFDaEI7QUFDRztPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCO0FBQ0Y7S0FDSSxXQUFXO0tBQ1gsZUFBZTs7S0FFZixjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxnQkFBZ0I7Q0FDcEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLFdBQVc7S0FDWCxlQUFlO0tBQ2YsaUJBQWlCOztDQUVyQjtBQUNBO0tBQ0ksYUFBYTtLQUNiO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluQmd7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvYm9vay5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogMTMwdmg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5hZGRib29rZm9ybXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBtYXJnaW4tdG9wOjEwMHB4O1xuICAgIG1hcmdpbi1sZWZ0OjMzJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiA5NSU7XG4gICAgd2lkdGg6IDM1JTtcbiAgICBzY3JvbGwtYmVoYXZpb3I6IGF1dG87XG4gICAgXG59XG4uYm9va0ljb257XG4gICAgbWFyZ2luLXRvcDo1JTsgXG4gICAgY29sb3I6IHJnYigxMDEsIDIwMSwgMjM1KTtcblxufVxuLmJvb2tQe1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIDtcbiAgICBtYXJnaW4tbGVmdDo4LjUlO1xuICAgIG1hcmdpbi10b3A6LTEwJTtcbiAgICBwYWRkaW5nLWxlZnQ6MTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrIDtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5hZGRyZXNze1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgXG59XG4uYm9va0ljb24xe1xuICAgIG1hcmdpbi10b3A6LTIlOyBcbiAgICBjb2xvcjogcmdiKDEwMSwgMjAxLCAyMzUpO1xuXG59XG4uYm9va1Axe1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIDtcbiAgICBtYXJnaW4tbGVmdDo4LjUlO1xuICAgIG1hcmdpbi10b3A6LTEwJTtcbiAgICBwYWRkaW5nLWxlZnQ6MTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrIDtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cblxuLmZvb3RlciB7XG4gXG4gICAgbWFyZ2luLWxlZnQ6NiU7XG4gICAgXG4gICAgIG1hcmdpbi10b3A6NjUlO1xuICAgICBtYXJnaW4tYm90dG9tOiAyJTtcbiAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgYm90dG9tOiAwO1xuICAgICB3aWR0aDogODklO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICBib3JkZXItcmFkaXVzOjVweDtcbiAgIH1cbiAgIC5mb290ZXIgYWRkcmVzc3tcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbi10b3A6LTklO1xuICAgIG1hcmdpbi1yaWdodDozMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ubWFpbHVze1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6LTIlO1xuIHBhZGRpbmc6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OjYzJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi52bCB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgYmxhY2s7XG4gIGhlaWdodDogODNweDtcbiAgbWFyZ2luLXJpZ2h0OjEwJTtcbiAgbWFyZ2luLWxlZnQ6LTYlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6MHB4O1xufVxuICAgLmZvb3Rlci1pY29ue1xuICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgIG1hcmdpbi10b3A6MTVweDtcbiAgICAgICBtYXJnaW4tbGVmdDoxNXB4O1xuICAgfVxuIC5sb2dvLWZvb3RlcntcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIG1hcmdpbi10b3A6MjFweDtcbiAgICAgXG4gICAgIGZvbnQtc2l6ZToyMHB4O1xuIH1cbiAuZmItaWNvbntcbiAgICAgbWFyZ2luLWxlZnQ6LTEwJTtcbiB9XG4gI3NvY2lhbC1mYjpob3ZlciB7XG4gICAgIGNvbG9yOiAjM0I1OTk4O1xuIH1cbiAjc29jaWFsLXR3OmhvdmVyIHtcbiAgICAgY29sb3I6ICM0MDk5RkY7XG4gfVxuICNzb2NpYWwtZ3A6aG92ZXIge1xuICAgICBjb2xvcjogI2QzNDgzNjtcbiB9XG4gI3NvY2lhbC1lbTpob3ZlciB7XG4gICAgIGNvbG9yOiAjZjM5YzEyO1xuIH1cbiAuc29jaWFsLWljb25ze1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICBtYXJnaW4tbGVmdDoxNzBweDtcbiAgICAgXG4gfVxuIC5zb2NpYWwtaWNvbnMgYXtcbiAgICAgcGFkZGluZzogMTBweDtcbiAgICAgY29sb3I6cmdiKDkwLCAyMDEsIDIzOClcbiB9Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-contact',
            templateUrl: './contact.component.html',
            styleUrls: ['./contact.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/home/home.component.ts":
    /*!****************************************!*\
      !*** ./src/app/home/home.component.ts ***!
      \****************************************/

    /*! exports provided: HomeComponent */

    /***/
    function srcAppHomeHomeComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeComponent", function () {
        return HomeComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var HomeComponent = /*#__PURE__*/function () {
        function HomeComponent(router) {
          _classCallCheck(this, HomeComponent);

          this.router = router;
          this.load = 0;
        }

        _createClass(HomeComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log("In HOME ");
          }
        }]);

        return HomeComponent;
      }();

      HomeComponent.ɵfac = function HomeComponent_Factory(t) {
        return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]));
      };

      HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HomeComponent,
        selectors: [["app-home"]],
        decls: 34,
        vars: 0,
        consts: [[1, "banner_w3lspvt"], [1, "st-slider"], ["type", "radio", "name", "slider", "id", "play1", "checked", "", 1, "cs_anchor", "radio"], ["type", "radio", "name", "slider", "id", "slide1", 1, "cs_anchor", "radio"], ["type", "radio", "name", "slider", "id", "slide2", 1, "cs_anchor", "radio"], ["type", "radio", "name", "slider", "id", "slide3", 1, "cs_anchor", "radio"], [1, "images"], [1, "images-inner"], [1, "image-slide"], [1, "banner-w3ls-1"], [1, "banner-w3ls-2"], [1, "banner-w3ls-3"], [1, "labels"], [1, "fake-radio"], ["for", "slide1", 1, "radio-btn"], ["for", "slide2", 1, "radio-btn"], ["for", "slide3", 1, "radio-btn"], [1, "banner-text"], ["href", "index.html", 1, "logo"], [1, "banner-sub-txt"], [1, "banner-txt"], [1, "banner-txt1"], [1, "fas", "fa-book-open", "fa-2x"], ["routerLink", "about", 1, "btn", "button-style", "mt-5"], [1, "footer"]],
        template: function HomeComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "input", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "input", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "label", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "label", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "fieldset");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "legend");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "BookZZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h1", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "There is none as loyal as a Book");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "The journey of a lifetime starts with the turning of a page.......");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "i", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Read More");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]],
        styles: [".banner_w3lspvt[_ngcontent-%COMP%] {\n    position: relative;\n    z-index: 1;\n  \n}\n\n.banner-text[_ngcontent-%COMP%] {\n    text-align: center;\n    padding: 11vw 0 14vw;\n    max-width: 800px;\n    margin: 0 auto;\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    \n}\nfieldset[_ngcontent-%COMP%] {\n    padding: 1em;\n    border: 5px solid rgba(255, 255, 255, 1.7);\n    border-radius: 30px;\n    position: relative;\n    height: 3%;\n}\nlegend[_ngcontent-%COMP%] {\n    max-width: 500px;\n    margin: 0 auto;\n}\na.logo[_ngcontent-%COMP%] {\n    color: #fff;\n    letter-spacing: 4px;\n    font-size: 4.5em;\n    text-shadow: 2px 5px 3px rgba(0, 0, 0, 0.27);\n    font-family: 'Tangerine', cursive;\n    font-weight: bold;\n    text-decoration: none;\n}\n.banner-text[_ngcontent-%COMP%]   h1.banner-txt[_ngcontent-%COMP%] {\n    color: rgba(255, 255, 255, 0.6);\n    font-weight: bold;\n    letter-spacing: 2px;\n    font-size: 2em;\n    text-transform: uppercase;\n    position: relative;\n}\n.banner-text[_ngcontent-%COMP%]   h1.banner-txt[_ngcontent-%COMP%]:after {\n    position: absolute;\n    background: rgba(255, 255, 255, 0.6);\n    content: \"\";\n    width: 200px;\n    height: 1px;\n    bottom: -13px;\n    left: 0;\n    right: 0;\n    margin: 0 auto;\n}\n.banner-text[_ngcontent-%COMP%]   p.banner-txt1[_ngcontent-%COMP%] {\n    color: #fff;\n    text-transform: uppercase;\n    font-size: 1.2em;\n    letter-spacing: 2px;\n    font-weight: 500;\n    margin: 2em auto 3em;\n    max-width: 700px;\n}\na.button-style[_ngcontent-%COMP%] {\n    padding: 13px 26px;\n    color: #000;\n    letter-spacing: 1px;\n    border-radius: 0px;\n    -webkit-border-radius: 0px;\n    -o-border-radius: 0px;\n    -ms-border-radius: 0px;\n    -moz-border-radius: 0px;\n    border: 1px solid #fff;\n    font-size: 15px;\n    background: #fff;\n    text-transform: uppercase;\n    position: absolute;\n    bottom: -27px;\n    left: 0;\n    right: 0;\n    margin: 0 auto;\n    text-align: center;\n    display: inline-block;\n    max-width: 150px;\n}\n\n.radio[_ngcontent-%COMP%] {\n    display: none;\n}\n.images[_ngcontent-%COMP%] {\n    overflow: hidden;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    width: 100%;\n}\n.images-inner[_ngcontent-%COMP%] {\n    width: 500%;\n    transition: all 800ms cubic-bezier(0.770, 0.000, 0.175, 1.000);\n    transition-timing-function: cubic-bezier(0.770, 0.000, 0.175, 1.000);\n}\n.image-slide[_ngcontent-%COMP%] {\n    width: 20%;\n    float: left;\n}\n.image-slide[_ngcontent-%COMP%], .fake-radio[_ngcontent-%COMP%], .radio-btn[_ngcontent-%COMP%] {\n    transition: all 0.5s ease-out;\n}\n.fake-radio[_ngcontent-%COMP%] {\n    text-align: center;\n    position: absolute;\n    bottom: 5%;\n    left: 0;\n    right: 0;\n    z-index: 9;\n}\n\n#slide1[_ngcontent-%COMP%]:checked ~ .images[_ngcontent-%COMP%]   .images-inner[_ngcontent-%COMP%] {\n    margin-left: 0;\n}\n#slide2[_ngcontent-%COMP%]:checked ~ .images[_ngcontent-%COMP%]   .images-inner[_ngcontent-%COMP%] {\n    margin-left: -100%;\n}\n#slide3[_ngcontent-%COMP%]:checked ~ .images[_ngcontent-%COMP%]   .images-inner[_ngcontent-%COMP%] {\n    margin-left: -200%;\n}\n\n#slide1[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(1), #slide2[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(2), #slide3[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(3) {\n    background: #ff3c41;\n}\n.radio-btn[_ngcontent-%COMP%] {\n    width: 10px;\n    height: 10px;\n    border-radius: 5px;\n    background: #fff;\n    display: inline-block !important;\n    margin: 0 1px;\n    cursor: pointer;\n}\n\n\n#slide1[_ngcontent-%COMP%]:checked ~ .labels[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%]:nth-child(1), #slide2[_ngcontent-%COMP%]:checked ~ .labels[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%]:nth-child(2), #slide3[_ngcontent-%COMP%]:checked ~ .labels[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%]:nth-child(3) {\n    opacity: 1;\n}\n.label[_ngcontent-%COMP%] {\n    opacity: 0;\n    position: absolute;\n}\n\n\n@-webkit-keyframes bullet {\n\n    0%,\n    33.32333333333334% {\n        background: red;\n    }\n\n    33.333333333333336%,\n    100% {\n        background: gray;\n    }\n}\n@keyframes bullet {\n\n    0%,\n    33.32333333333334% {\n        background: red;\n    }\n\n    33.333333333333336%,\n    100% {\n        background: gray;\n    }\n}\n#play1[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(1) {\n    -webkit-animation: bullet 12300ms infinite -1000ms;\n            animation: bullet 12300ms infinite -1000ms;\n}\n#play1[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(2) {\n    -webkit-animation: bullet 12300ms infinite 3100ms;\n            animation: bullet 12300ms infinite 3100ms;\n}\n#play1[_ngcontent-%COMP%]:checked ~ div[_ngcontent-%COMP%]   .fake-radio[_ngcontent-%COMP%]   .radio-btn[_ngcontent-%COMP%]:nth-child(3) {\n    -webkit-animation: bullet 12300ms infinite 7200ms;\n            animation: bullet 12300ms infinite 7200ms;\n}\n\n\n@-webkit-keyframes slide {\n\n    0%,\n    25.203252032520325% {\n        margin-left: 0;\n    }\n\n    33.333333333333336%,\n    58.53658536585366% {\n        margin-left: -100%;\n    }\n\n    66.66666666666667%,\n    91.869918699187% {\n        margin-left: -200%;\n    }\n}\n@keyframes slide {\n\n    0%,\n    25.203252032520325% {\n        margin-left: 0;\n    }\n\n    33.333333333333336%,\n    58.53658536585366% {\n        margin-left: -100%;\n    }\n\n    66.66666666666667%,\n    91.869918699187% {\n        margin-left: -200%;\n    }\n}\n.st-slider[_ngcontent-%COMP%] > #play1[_ngcontent-%COMP%]:checked ~ .images[_ngcontent-%COMP%]   .images-inner[_ngcontent-%COMP%] {\n    -webkit-animation: slide 12300ms infinite;\n            animation: slide 12300ms infinite;\n \n}\n\n\n\n.banner-w3ls-1[_ngcontent-%COMP%] {\n    background: url('book7.png') no-repeat top;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 700px;\n}\n.banner-w3ls-2[_ngcontent-%COMP%] {\n    background: url('book2.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 700px;\n}\n.banner-w3ls-3[_ngcontent-%COMP%] {\n    background: url('book6.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 700px;\n}\n\n\n.banner_w3lspvt-2[_ngcontent-%COMP%] {\n    background: url('1.jpg') no-repeat center;\n    -ms-background-size: cover;\n    background-size: cover;\n    \n}\n.banner-inner[_ngcontent-%COMP%] {\n    padding: 4vw 0;\n}\n.footer[_ngcontent-%COMP%]{\n    height: 50px;\n    background-color:black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFLQSxXQUFXO0FBQ1g7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTs7QUFFZDtBQUlBLGdCQUFnQjtBQUNoQjtJQUNJLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxRQUFROztBQUVaO0FBRUE7SUFDSSxZQUFZO0lBQ1osMENBQTBDO0lBSzFDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsVUFBVTtBQUNkO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjtBQUVBO0lBQ0ksV0FBVztJQUNYLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsNENBQTRDO0lBQzVDLGlDQUFpQztJQUNqQyxpQkFBaUI7SUFDakIscUJBQXFCO0FBQ3pCO0FBRUE7SUFDSSwrQkFBK0I7SUFDL0IsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QseUJBQXlCO0lBQ3pCLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLG9DQUFvQztJQUNwQyxXQUFXO0lBQ1gsWUFBWTtJQUNaLFdBQVc7SUFDWCxhQUFhO0lBQ2IsT0FBTztJQUNQLFFBQVE7SUFDUixjQUFjO0FBQ2xCO0FBRUE7SUFDSSxXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIscUJBQXFCO0lBQ3JCLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsc0JBQXNCO0lBQ3RCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsT0FBTztJQUNQLFFBQVE7SUFDUixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixnQkFBZ0I7QUFDcEI7QUFFQSxrQkFBa0I7QUFDbEI7SUFDSSxhQUFhO0FBQ2pCO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsTUFBTTtJQUNOLFNBQVM7SUFDVCxPQUFPO0lBQ1AsUUFBUTtJQUNSLFdBQVc7QUFDZjtBQUVBO0lBQ0ksV0FBVztJQUNYLDhEQUE4RDtJQUM5RCxvRUFBb0U7QUFDeEU7QUFFQTtJQUNJLFVBQVU7SUFDVixXQUFXO0FBQ2Y7QUFFQTs7O0lBR0ksNkJBQTZCO0FBQ2pDO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixPQUFPO0lBQ1AsUUFBUTtJQUNSLFVBQVU7QUFDZDtBQUVBLHFDQUFxQztBQUNyQztJQUNJLGNBQWM7QUFDbEI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBRUEscUJBQXFCO0FBQ3JCOzs7SUFHSSxtQkFBbUI7QUFDdkI7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBS1osa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQ0FBZ0M7SUFDaEMsYUFBYTtJQUNiLGVBQWU7QUFDbkI7QUFFQSwyQkFBMkI7QUFFM0IsbUJBQW1CO0FBQ25COzs7SUFHSSxVQUFVO0FBQ2Q7QUFFQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7QUFDdEI7QUFFQSx5QkFBeUI7QUFFekIsbUNBQW1DO0FBQ25DOztJQUVJOztRQUVJLGVBQWU7SUFDbkI7O0lBRUE7O1FBRUksZ0JBQWdCO0lBQ3BCO0FBQ0o7QUFYQTs7SUFFSTs7UUFFSSxlQUFlO0lBQ25COztJQUVBOztRQUVJLGdCQUFnQjtJQUNwQjtBQUNKO0FBRUE7SUFDSSxrREFBMEM7WUFBMUMsMENBQTBDO0FBQzlDO0FBRUE7SUFDSSxpREFBeUM7WUFBekMseUNBQXlDO0FBQzdDO0FBRUE7SUFDSSxpREFBeUM7WUFBekMseUNBQXlDO0FBQzdDO0FBRUEseUNBQXlDO0FBRXpDLGtDQUFrQztBQUNsQzs7SUFFSTs7UUFFSSxjQUFjO0lBQ2xCOztJQUVBOztRQUVJLGtCQUFrQjtJQUN0Qjs7SUFFQTs7UUFFSSxrQkFBa0I7SUFDdEI7QUFDSjtBQWhCQTs7SUFFSTs7UUFFSSxjQUFjO0lBQ2xCOztJQUVBOztRQUVJLGtCQUFrQjtJQUN0Qjs7SUFFQTs7UUFFSSxrQkFBa0I7SUFDdEI7QUFDSjtBQUVBO0lBQ0kseUNBQWlDO1lBQWpDLGlDQUFpQzs7QUFFckM7QUFFQSx3Q0FBd0M7QUFDeEMsb0JBQW9CO0FBRXBCLGlDQUFpQztBQUNqQztJQUNJLDBDQUE0RDtJQUM1RCxzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLGlCQUFpQjtBQUNyQjtBQUVBO0lBQ0ksNkNBQStEO0lBQy9ELHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsMkJBQTJCO0lBQzNCLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsaUJBQWlCO0FBQ3JCO0FBRUE7SUFDSSw2Q0FBK0Q7SUFDL0Qsc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QiwyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQixpQkFBaUI7QUFDckI7QUFFQSxtQ0FBbUM7QUFDbkMsa0JBQWtCO0FBS2xCO0lBQ0kseUNBQWlEO0lBSWpELDBCQUEwQjtJQUMxQixzQkFBc0I7O0FBRTFCO0FBRUE7SUFDSSxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osc0JBQXNCO0FBQzFCIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuXG4gXG5cbi8qIGJhbm5lciAqL1xuLmJhbm5lcl93M2xzcHZ0IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgXG59XG5cblxuXG4vKiBiYW5uZXItdGV4dCAqL1xuLmJhbm5lci10ZXh0IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMTF2dyAwIDE0dnc7XG4gICAgbWF4LXdpZHRoOiA4MDBweDtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgXG59XG5cbmZpZWxkc2V0IHtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAxLjcpO1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgLW8tYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBoZWlnaHQ6IDMlO1xufVxuXG5sZWdlbmQge1xuICAgIG1heC13aWR0aDogNTAwcHg7XG4gICAgbWFyZ2luOiAwIGF1dG87XG59XG5cbmEubG9nbyB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDRweDtcbiAgICBmb250LXNpemU6IDQuNWVtO1xuICAgIHRleHQtc2hhZG93OiAycHggNXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMjcpO1xuICAgIGZvbnQtZmFtaWx5OiAnVGFuZ2VyaW5lJywgY3Vyc2l2ZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5iYW5uZXItdGV4dCBoMS5iYW5uZXItdHh0IHtcbiAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgZm9udC1zaXplOiAyZW07XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5iYW5uZXItdGV4dCBoMS5iYW5uZXItdHh0OmFmdGVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIGhlaWdodDogMXB4O1xuICAgIGJvdHRvbTogLTEzcHg7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmJhbm5lci10ZXh0IHAuYmFubmVyLXR4dDEge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAxLjJlbTtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbWFyZ2luOiAyZW0gYXV0byAzZW07XG4gICAgbWF4LXdpZHRoOiA3MDBweDtcbn1cblxuYS5idXR0b24tc3R5bGUge1xuICAgIHBhZGRpbmc6IDEzcHggMjZweDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAtby1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTI3cHg7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1heC13aWR0aDogMTUwcHg7XG59XG5cbi8qIGJhbm5lciBzbGlkZXIgKi9cbi5yYWRpbyB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLmltYWdlcyB7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5pbWFnZXMtaW5uZXIge1xuICAgIHdpZHRoOiA1MDAlO1xuICAgIHRyYW5zaXRpb246IGFsbCA4MDBtcyBjdWJpYy1iZXppZXIoMC43NzAsIDAuMDAwLCAwLjE3NSwgMS4wMDApO1xuICAgIHRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOiBjdWJpYy1iZXppZXIoMC43NzAsIDAuMDAwLCAwLjE3NSwgMS4wMDApO1xufVxuXG4uaW1hZ2Utc2xpZGUge1xuICAgIHdpZHRoOiAyMCU7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5pbWFnZS1zbGlkZSxcbi5mYWtlLXJhZGlvLFxuLnJhZGlvLWJ0biB7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1vdXQ7XG59XG5cbi5mYWtlLXJhZGlvIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogNSU7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICB6LWluZGV4OiA5O1xufVxuXG4vKiBNb3ZlIHNsaWRlcyBvdmVyZmxvd2VkIGNvbnRhaW5lciAqL1xuI3NsaWRlMTpjaGVja2Vkfi5pbWFnZXMgLmltYWdlcy1pbm5lciB7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG59XG5cbiNzbGlkZTI6Y2hlY2tlZH4uaW1hZ2VzIC5pbWFnZXMtaW5uZXIge1xuICAgIG1hcmdpbi1sZWZ0OiAtMTAwJTtcbn1cblxuI3NsaWRlMzpjaGVja2Vkfi5pbWFnZXMgLmltYWdlcy1pbm5lciB7XG4gICAgbWFyZ2luLWxlZnQ6IC0yMDAlO1xufVxuXG4vKiBDb2xvciBvZiBidWxsZXRzICovXG4jc2xpZGUxOmNoZWNrZWR+ZGl2IC5mYWtlLXJhZGlvIC5yYWRpby1idG46bnRoLWNoaWxkKDEpLFxuI3NsaWRlMjpjaGVja2VkfmRpdiAuZmFrZS1yYWRpbyAucmFkaW8tYnRuOm50aC1jaGlsZCgyKSxcbiNzbGlkZTM6Y2hlY2tlZH5kaXYgLmZha2UtcmFkaW8gLnJhZGlvLWJ0bjpudGgtY2hpbGQoMykge1xuICAgIGJhY2tncm91bmQ6ICNmZjNjNDE7XG59XG5cbi5yYWRpby1idG4ge1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGhlaWdodDogMTBweDtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtby1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcbiAgICBtYXJnaW46IDAgMXB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLyogQ29sb3Igb2YgYnVsbGV0cyAtIEVORCAqL1xuXG4vKiBUZXh0IG9mIHNsaWRlcyAqL1xuI3NsaWRlMTpjaGVja2Vkfi5sYWJlbHMgLmxhYmVsOm50aC1jaGlsZCgxKSxcbiNzbGlkZTI6Y2hlY2tlZH4ubGFiZWxzIC5sYWJlbDpudGgtY2hpbGQoMiksXG4jc2xpZGUzOmNoZWNrZWR+LmxhYmVscyAubGFiZWw6bnRoLWNoaWxkKDMpIHtcbiAgICBvcGFjaXR5OiAxO1xufVxuXG4ubGFiZWwge1xuICAgIG9wYWNpdHk6IDA7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4vKiBUZXh0IG9mIHNsaWRlcyAtIEVORCAqL1xuXG4vKiBDYWxjdWxhdGUgQVVUT1BMQVkgZm9yIEJVTExFVFMgKi9cbkBrZXlmcmFtZXMgYnVsbGV0IHtcblxuICAgIDAlLFxuICAgIDMzLjMyMzMzMzMzMzMzMzM0JSB7XG4gICAgICAgIGJhY2tncm91bmQ6IHJlZDtcbiAgICB9XG5cbiAgICAzMy4zMzMzMzMzMzMzMzMzMzYlLFxuICAgIDEwMCUge1xuICAgICAgICBiYWNrZ3JvdW5kOiBncmF5O1xuICAgIH1cbn1cblxuI3BsYXkxOmNoZWNrZWR+ZGl2IC5mYWtlLXJhZGlvIC5yYWRpby1idG46bnRoLWNoaWxkKDEpIHtcbiAgICBhbmltYXRpb246IGJ1bGxldCAxMjMwMG1zIGluZmluaXRlIC0xMDAwbXM7XG59XG5cbiNwbGF5MTpjaGVja2VkfmRpdiAuZmFrZS1yYWRpbyAucmFkaW8tYnRuOm50aC1jaGlsZCgyKSB7XG4gICAgYW5pbWF0aW9uOiBidWxsZXQgMTIzMDBtcyBpbmZpbml0ZSAzMTAwbXM7XG59XG5cbiNwbGF5MTpjaGVja2VkfmRpdiAuZmFrZS1yYWRpbyAucmFkaW8tYnRuOm50aC1jaGlsZCgzKSB7XG4gICAgYW5pbWF0aW9uOiBidWxsZXQgMTIzMDBtcyBpbmZpbml0ZSA3MjAwbXM7XG59XG5cbi8qIENhbGN1bGF0ZSBBVVRPUExBWSBmb3IgQlVMTEVUUyAtIEVORCAqL1xuXG4vKiBDYWxjdWxhdGUgQVVUT1BMQVkgZm9yIFNMSURFUyAqL1xuQGtleWZyYW1lcyBzbGlkZSB7XG5cbiAgICAwJSxcbiAgICAyNS4yMDMyNTIwMzI1MjAzMjUlIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgfVxuXG4gICAgMzMuMzMzMzMzMzMzMzMzMzM2JSxcbiAgICA1OC41MzY1ODUzNjU4NTM2NiUge1xuICAgICAgICBtYXJnaW4tbGVmdDogLTEwMCU7XG4gICAgfVxuXG4gICAgNjYuNjY2NjY2NjY2NjY2NjclLFxuICAgIDkxLjg2OTkxODY5OTE4NyUge1xuICAgICAgICBtYXJnaW4tbGVmdDogLTIwMCU7XG4gICAgfVxufVxuXG4uc3Qtc2xpZGVyPiNwbGF5MTpjaGVja2Vkfi5pbWFnZXMgLmltYWdlcy1pbm5lciB7XG4gICAgYW5pbWF0aW9uOiBzbGlkZSAxMjMwMG1zIGluZmluaXRlO1xuIFxufVxuXG4vKiBDYWxjdWxhdGUgQVVUT1BMQVkgZm9yIFNMSURFUyAtIEVORCAqL1xuLyogLy9iYW5uZXIgc2xpZGVyICovXG5cbi8qIGJhY2tncm91bmQgaW1hZ2VzIGZvciBiYW5uZXIgKi9cbi5iYW5uZXItdzNscy0xIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rNy5wbmcpIG5vLXJlcGVhdCB0b3A7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogNzAwcHg7XG59XG5cbi5iYW5uZXItdzNscy0yIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rMi5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogNzAwcHg7XG59XG5cbi5iYW5uZXItdzNscy0zIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rNi5qcGcpIG5vLXJlcGVhdCBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWluLWhlaWdodDogNzAwcHg7XG59XG5cbi8qIC8vYmFja2dyb3VuZCBpbWFnZXMgZm9yIGJhbm5lciAqL1xuLyogLy9iYW5uZXIgdGV4dCAqL1xuXG5cblxuXG4uYmFubmVyX3czbHNwdnQtMiB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uL2ltYWdlcy8xLmpwZykgbm8tcmVwZWF0IGNlbnRlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1zLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBcbn1cblxuLmJhbm5lci1pbm5lciB7XG4gICAgcGFkZGluZzogNHZ3IDA7XG59XG4uZm9vdGVye1xuICAgIGhlaWdodDogNTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrO1xufVxuXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.css']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/page-not-found.component.ts":
    /*!*********************************************!*\
      !*** ./src/app/page-not-found.component.ts ***!
      \*********************************************/

    /*! exports provided: PageNotFoundComponent */

    /***/
    function srcAppPageNotFoundComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function () {
        return PageNotFoundComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PageNotFoundComponent = /*#__PURE__*/function () {
        function PageNotFoundComponent() {
          _classCallCheck(this, PageNotFoundComponent);
        }

        _createClass(PageNotFoundComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PageNotFoundComponent;
      }();

      PageNotFoundComponent.ɵfac = function PageNotFoundComponent_Factory(t) {
        return new (t || PageNotFoundComponent)();
      };

      PageNotFoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: PageNotFoundComponent,
        selectors: [["app-page-not-found"]],
        decls: 4,
        vars: 0,
        consts: [[1, "row"], [1, "col", 2, "margin", "26%"]],
        template: function PageNotFoundComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "This page is not available");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        encapsulation: 2
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PageNotFoundComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-page-not-found',
            templateUrl: './page-not-found.component.html'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/profile/profile.component.ts":
    /*!**********************************************!*\
      !*** ./src/app/profile/profile.component.ts ***!
      \**********************************************/

    /*! exports provided: ProfileComponent */

    /***/
    function srcAppProfileProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfileComponent", function () {
        return ProfileComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-show-hide-password */
      "./node_modules/ngx-show-hide-password/__ivy_ngcc__/fesm2015/ngx-show-hide-password.js");

      var ProfileComponent = /*#__PURE__*/function () {
        function ProfileComponent(_authService, location) {
          _classCallCheck(this, ProfileComponent);

          this._authService = _authService;
          this.location = location;
        }

        _createClass(ProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            if (this._authService.isLoggedIn) {
              this.user = JSON.parse(localStorage.getItem('currentUser'));
              this.fullname = this.user.fname + " " + this.user.lname;
            }
          }
        }, {
          key: "goBack",
          value: function goBack() {
            this.location.back();
          }
        }]);

        return ProfileComponent;
      }();

      ProfileComponent.ɵfac = function ProfileComponent_Factory(t) {
        return new (t || ProfileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]));
      };

      ProfileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ProfileComponent,
        selectors: [["app-profile"]],
        decls: 68,
        vars: 8,
        consts: [[1, "mainBg"], [1, "addbookform"], [1, "title"], [3, "ngSubmit"], [1, "form-group"], ["for", "exampleInputEmail1"], ["type", "text", "readonly", "", 1, "form-control", 2, "width", "40%", 3, "value"], [1, "form-group", 2, "margin-left", "45%", "margin-top", "-8.7%"], ["type", "text", "placeholder", "Last Name", "readonly", "", 1, "form-control", 2, "width", "80%", 3, "value"], ["type", "email", "id", "exampleInputEmail1", "aria-describedby", "emailHelp", "placeholder", "Enter email", "readonly", "", 1, "form-control", 3, "value"], ["id", "emailHelp", 1, "form-text", "text-muted"], ["for", "exampleInputPassword1"], ["size", "sm", "btnStyle", "primary", 3, "btnOutline"], ["type", "password", "name", "password", "readonly", "", 3, "value"], ["for", "contact"], ["type", "number", "id", "formGroupExampleInput", "readonly", "", 1, "form-control", 3, "value"], ["for", "location"], ["type", "text", "id", "formGroupExampleInput", "readonly", "", 1, "form-control", 3, "value"], ["type", "submit", 1, "btn", "btn-primary"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"]],
        template: function ProfileComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ProfileComponent_Template_form_ngSubmit_4_listener() {
              return ctx.goBack();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "First Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Email address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "small", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "We'll never share your email with anyone else.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "show-hide-password", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "input", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Contact");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "input", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "label", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Location");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "input", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Go Back");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "BookZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "a", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "i", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "i", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "i", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "i", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "div", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, " Flipkart Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, " Outer Ring Road, Devarabeesanahalli Village, ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Visit us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " Box 564, Disneyland");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, " USA ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.fullname, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.fname);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.lname);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.email);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("btnOutline", false);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.password);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.contact);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.user.location);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], ngx_show_hide_password__WEBPACK_IMPORTED_MODULE_4__["ShowHidePasswordComponent"]],
        styles: [".mainBg[_ngcontent-%COMP%]{\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 130vh;\n    overflow: hidden;\n}\n.addbookform[_ngcontent-%COMP%]{\n    background-color: white;\n    margin-top:100px;\n    margin-left:13%;\n    position: absolute;\n    height: 100%;\n    width: 75%;\n    scroll-behavior: auto;\n    \n}\n.title[_ngcontent-%COMP%]{\n    font-size:30px;\n    \n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.35);\n    align-content: center;\n    padding: 10px;\n    height: 70px;\n    text-align: center;\n    background-color: #A0DEE2;\n}\nform[_ngcontent-%COMP%]{\n    padding: 15px;\n}\n.footer[_ngcontent-%COMP%] {\n   margin-top:70%;\n   margin-left:6%;\n    margin-bottom: 2%;\n    height: 100px;\n    \n    width: 89%;\n    background-color: white;\n   \n    text-align: center;\n    border-radius:5px;\n  }\naddress[_ngcontent-%COMP%]{\n      float: right;\n      font-size: 12px;\n      margin-top:-7%;\n      margin-right:30px;\n      text-align: left;\n  }\n.mailus[_ngcontent-%COMP%]{\n    \n    font-size: 12px;\n    margin-top:-2%;\n    margin-left:63%;\n    text-align: left;\n  }\n.vl[_ngcontent-%COMP%] {\n    border-left: 1px solid black;\n    height: 12%;\n    margin-right:10%;\n    margin-left:-6%;\n    position: absolute;\n    margin-top:10px;\n  }\n.footer-icon[_ngcontent-%COMP%]{\n      float: left;\n      margin-top:15px;\n      margin-left:15px;\n  }\n.logo-footer[_ngcontent-%COMP%]{\n    float: left;\n    margin-top:21px;\n    \n    font-size:20px;\n}\n.fb-icon[_ngcontent-%COMP%]{\n    margin-left:-10%;\n}\n#social-fb[_ngcontent-%COMP%]:hover {\n    color: #3B5998;\n}\n#social-tw[_ngcontent-%COMP%]:hover {\n    color: #4099FF;\n}\n#social-gp[_ngcontent-%COMP%]:hover {\n    color: #d34836;\n}\n#social-em[_ngcontent-%COMP%]:hover {\n    color: #f39c12;\n}\n.social-icons[_ngcontent-%COMP%]{\n    float: left;\n    margin-top:20px;\n    margin-left:170px;\n    \n}\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n    padding: 10px;\n    color:rgb(90, 201, 238)\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0Q0FBOEQ7SUFDOUQsc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QiwyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFVBQVU7SUFDVixxQkFBcUI7O0FBRXpCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsNkJBQTZCO0lBQzdCLDZDQUE2QztJQUM3QyxxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSxhQUFhO0FBQ2pCO0FBRUE7R0FDRyxjQUFjO0dBQ2QsY0FBYztJQUNiLGlCQUFpQjtJQUNqQixhQUFhOztJQUViLFVBQVU7SUFDVix1QkFBdUI7O0lBRXZCLGtCQUFrQjtJQUNsQixpQkFBaUI7RUFDbkI7QUFDQTtNQUNJLFlBQVk7TUFDWixlQUFlO01BQ2YsY0FBYztNQUNkLGlCQUFpQjtNQUNqQixnQkFBZ0I7RUFDcEI7QUFDQTs7SUFFRSxlQUFlO0lBQ2YsY0FBYztJQUNkLGVBQWU7SUFDZixnQkFBZ0I7RUFDbEI7QUFDQTtJQUNFLDRCQUE0QjtJQUM1QixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZUFBZTtFQUNqQjtBQUNBO01BQ0ksV0FBVztNQUNYLGVBQWU7TUFDZixnQkFBZ0I7RUFDcEI7QUFDRjtJQUNJLFdBQVc7SUFDWCxlQUFlOztJQUVmLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixpQkFBaUI7O0FBRXJCO0FBQ0E7SUFDSSxhQUFhO0lBQ2I7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW5CZ3tcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rLmpwZykgbm8tcmVwZWF0IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW8tYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbXMtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBtaW4taGVpZ2h0OiAxMzB2aDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmFkZGJvb2tmb3Jte1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6MTAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6MTMlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDc1JTtcbiAgICBzY3JvbGwtYmVoYXZpb3I6IGF1dG87XG4gICAgXG59XG4udGl0bGV7XG4gICAgZm9udC1zaXplOjMwcHg7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgYmxhY2s7ICovXG4gICAgYm94LXNoYWRvdzogMCAxNXB4IDI1cHggLTEwcHggcmdiYSgwLDAsMCwuMzUpO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGhlaWdodDogNzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0EwREVFMjtcbn1cbmZvcm17XG4gICAgcGFkZGluZzogMTVweDtcbn1cblxuLmZvb3RlciB7XG4gICBtYXJnaW4tdG9wOjcwJTtcbiAgIG1hcmdpbi1sZWZ0OjYlO1xuICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgXG4gICAgd2lkdGg6IDg5JTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgIFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOjVweDtcbiAgfVxuICBhZGRyZXNze1xuICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgbWFyZ2luLXRvcDotNyU7XG4gICAgICBtYXJnaW4tcmlnaHQ6MzBweDtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLm1haWx1c3tcbiAgICBcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbWFyZ2luLXRvcDotMiU7XG4gICAgbWFyZ2luLWxlZnQ6NjMlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgLnZsIHtcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkIGJsYWNrO1xuICAgIGhlaWdodDogMTIlO1xuICAgIG1hcmdpbi1yaWdodDoxMCU7XG4gICAgbWFyZ2luLWxlZnQ6LTYlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tdG9wOjEwcHg7XG4gIH1cbiAgLmZvb3Rlci1pY29ue1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBtYXJnaW4tdG9wOjE1cHg7XG4gICAgICBtYXJnaW4tbGVmdDoxNXB4O1xuICB9XG4ubG9nby1mb290ZXJ7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXRvcDoyMXB4O1xuICAgIFxuICAgIGZvbnQtc2l6ZToyMHB4O1xufVxuLmZiLWljb257XG4gICAgbWFyZ2luLWxlZnQ6LTEwJTtcbn1cbiNzb2NpYWwtZmI6aG92ZXIge1xuICAgIGNvbG9yOiAjM0I1OTk4O1xufVxuI3NvY2lhbC10dzpob3ZlciB7XG4gICAgY29sb3I6ICM0MDk5RkY7XG59XG4jc29jaWFsLWdwOmhvdmVyIHtcbiAgICBjb2xvcjogI2QzNDgzNjtcbn1cbiNzb2NpYWwtZW06aG92ZXIge1xuICAgIGNvbG9yOiAjZjM5YzEyO1xufVxuLnNvY2lhbC1pY29uc3tcbiAgICBmbG9hdDogbGVmdDtcbiAgICBtYXJnaW4tdG9wOjIwcHg7XG4gICAgbWFyZ2luLWxlZnQ6MTcwcHg7XG4gICAgXG59XG4uc29jaWFsLWljb25zIGF7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBjb2xvcjpyZ2IoOTAsIDIwMSwgMjM4KVxufVxuXG4gXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProfileComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-profile',
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.css']
          }]
        }], function () {
          return [{
            type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
          }, {
            type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/register/register.component.ts":
    /*!************************************************!*\
      !*** ./src/app/register/register.component.ts ***!
      \************************************************/

    /*! exports provided: RegisterComponent */

    /***/
    function srcAppRegisterRegisterComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterComponent", function () {
        return RegisterComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _confirm_equal_validator_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../confirm-equal-validator.directive */
      "./src/app/confirm-equal-validator.directive.ts");

      function RegisterComponent_br_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_17_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " first name is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_17_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " This is not the specified pattern");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_17_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_17_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_17_div_2_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.pattern);
        }
      }

      function RegisterComponent_br_27_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_28_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " last name required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_28_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " This is not the specified pattern");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_28_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_28_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_28_div_2_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r4.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r4.errors.pattern);
        }
      }

      function RegisterComponent_br_39_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_40_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " email is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_40_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " email is invalid");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_40_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_40_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_40_div_2_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r7.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r7.errors.email);
        }
      }

      function RegisterComponent_br_51_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_52_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " password is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_52_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " password is invalid");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_52_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_52_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_52_div_2_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r10.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r10.errors.pattern);
        }
      }

      function RegisterComponent_br_61_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_62_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " confirm password is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_62_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " password and confirm password doesn't match");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_62_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_62_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_62_div_2_Template, 4, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](58);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.errors.notEqual);
        }
      }

      function RegisterComponent_br_73_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        }
      }

      function RegisterComponent_div_74_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "contact number is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_74_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "contact number is invalid");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RegisterComponent_div_74_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RegisterComponent_div_74_div_1_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RegisterComponent_div_74_div_2_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](70);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r16.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r16.errors.pattern);
        }
      }

      function RegisterComponent_option_82_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var location_r32 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", location_r32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", location_r32, " ");
        }
      }

      var RegisterComponent = /*#__PURE__*/function () {
        function RegisterComponent(_registerService, router) {
          _classCallCheck(this, RegisterComponent);

          this._registerService = _registerService;
          this.router = router;
          this.locations = ["Mumbai", "Pune", "Bangalore", "Kochi", "Gurgram", "Chennai", "Hyderabad"];
        }

        _createClass(RegisterComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "register",
          value: function register(formValue) {
            var _this = this;

            var newUser = {
              fname: formValue.fname,
              lname: formValue.lname,
              email: formValue.email,
              password: formValue.password,
              contact: formValue.contactno,
              location: formValue.location
            };

            this._registerService.addUser(newUser).subscribe(function (user) {
              // console.log(user);
              // localStorage.setItem('currentUser', JSON.stringify(user));
              _this.router.navigate(['/signin']);
            });
          }
        }]);

        return RegisterComponent;
      }();

      RegisterComponent.ɵfac = function RegisterComponent_Factory(t) {
        return new (t || RegisterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]));
      };

      RegisterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RegisterComponent,
        selectors: [["app-register"]],
        decls: 132,
        vars: 15,
        consts: [[1, "main-bg"], [1, "general"], [1, "title"], [1, "far", "fa-smile"], [1, "form1", 3, "ngSubmit"], ["formRef", "ngForm"], ["for", "fname"], [1, "required", 2, "color", "red"], ["type", "text", "name", "fname", "pattern", "[a-zA-Z]{1,15}", "title", "Username should only contain lowercase letters. e.g. john", "placeholder", "firstname", "ngModel", "", "required", ""], ["fnameRef", "ngModel"], [4, "ngIf"], ["for", "lname"], ["type", "text", "name", "lname", "pattern", "[a-zA-Z]{1,15}", "ngModel", "", "placeholder", "lastname", "required", ""], ["lnameRef", "ngModel"], [1, "inputIcon", "iconBg"], ["for", "email"], ["type", "email", "name", "email", "ngModel", "", "placeholder", "email", "required", "", "email", ""], ["emailRef", "ngModel"], [1, "fas", "fa-envelope"], ["for", "password"], ["type", "password", "name", "password", "pattern", "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})", "ngModel", "", "placeholder", "password", "required", ""], ["passwordRef", "ngModel"], [1, "fas", "fa-unlock-alt"], ["for", "cPassword"], ["type", "password", "name", "cPassword", "ngModel", "", "placeholder", "password", "appConfirmEqualValidator", "password", "required", ""], ["cpasswordRef", "ngModel"], ["for", "contactno"], ["type", "tel", "pattern", "(7|8|9)\\d{9}$", "name", "contactno", "ngModel", "", "placeholder", "contactno.", "required", ""], ["contactRef", "ngModel"], [1, "fas", "fa-phone-alt"], ["for", "location"], ["name", "location", "required", "", 3, "ngModel", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], [1, "fas", "fa-home"], ["type", "submit", 1, "btn", "btn-success", 3, "disabled"], [1, "contact"], [1, "registerBox"], [1, "specification"], [1, "section"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"], [2, "color", "red"], [3, "value"]],
        template: function RegisterComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Lets Begin! ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 4, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function RegisterComponent_Template_form_ngSubmit_5_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r33);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);

              return ctx.register(_r0.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "First Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 8, 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, RegisterComponent_br_16_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, RegisterComponent_div_17_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "input", 12, 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, RegisterComponent_br_27_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, RegisterComponent_div_28_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "label", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "input", 16, 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "i", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](39, RegisterComponent_br_39_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, RegisterComponent_div_40_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "input", 20, 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "i", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](51, RegisterComponent_br_51_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](52, RegisterComponent_div_52_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Confirm Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "input", 24, 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "i", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](61, RegisterComponent_br_61_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](62, RegisterComponent_div_62_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "label", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Contact no.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "input", 27, 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](73, RegisterComponent_br_73_Template, 1, 0, "br", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](74, RegisterComponent_div_74_Template, 3, 2, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "label", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Location");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "select", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function RegisterComponent_Template_select_ngModelChange_81_listener($event) {
              return ctx.locations[0] = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](82, RegisterComponent_option_82_Template, 2, 2, "option", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "i", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "button", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "register");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "p", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Form Specifications:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "section", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "ul");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, " Name should contain only letters or numbers. ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Password should be of length 6 and contains atllease 1 lowercase, 1 uppercase,1 digit and 1 special symbol.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "contach number should be of 10 digits");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "i", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "p", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "BookZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div", 42);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "a", 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "i", 44);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "a", 45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "i", 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "a", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "i", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "a", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "i", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 51);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "div", 52);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, " Box 564, Disneyland");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, " Bangalore ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Visit us at");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, " BookZone Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](127, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);

            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](14);

            var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](25);

            var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36);

            var _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](48);

            var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](58);

            var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](70);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r1.invalid && (_r1.dirty || _r1.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r4.invalid && (_r4.dirty || _r4.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r4.invalid && (_r4.dirty || _r4.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r7.invalid && (_r7.dirty || _r7.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r7.invalid && (_r7.dirty || _r7.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r10.invalid && (_r10.dirty || _r10.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r10.invalid && (_r10.dirty || _r10.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r13.invalid && (_r13.dirty || _r13.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.invalid && (_r13.dirty || _r13.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !(_r16.invalid && (_r16.dirty || _r16.touched)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r16.invalid && (_r16.dirty || _r16.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.locations[0]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.locations);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r0.invalid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["EmailValidator"], _confirm_equal_validator_directive__WEBPACK_IMPORTED_MODULE_5__["ConfirmEqualValidatorDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_x"]],
        styles: [".main-bg[_ngcontent-%COMP%] {\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 100vh;\n    overflow: hidden;\n    \n}\n.title[_ngcontent-%COMP%]{\n    margin-left: 10%;\n    margin-top:5%;\n}\n.general[_ngcontent-%COMP%] {\n    height: 800px;\n    width: 495px;\n    margin-top:9%;\n    margin-left:15%;\n    position: absolute;\n    background-color: white;\n    \n}\n.general[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n    font-size: 14px;\n    margin-bottom: 0px;\n    margin-top: 0px;\n}\n.contact[_ngcontent-%COMP%]{\n    height: 800px;\n    width: 495px;\n    margin-top:9%;\n    margin-left:51%;\n    position: absolute;\n    background-color: #7CB7C6;\n\n}\n.form1[_ngcontent-%COMP%]{\n    margin-left: 10%;\n    margin-top: 3%;\n}\n.general[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], select[_ngcontent-%COMP%]{\n    height: 30px;\n    width: 75%;\n    border:2px solid #aaa;\n    border-radius:4px;\n    margin:3px 0;\n    outline: none;\n    padding:5px;\n    box-sizing:border-box;\n    transition: .3s;\n    font-size:16px;\n   \n}\n.general[_ngcontent-%COMP%]   .form1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    font-size:12px;\n    font-style: italic\n}\nselect[_ngcontent-%COMP%]{\n  height: 34px;\n}\n.general[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus, select[_ngcontent-%COMP%]:focus{\n    border-color:#32949F;\n    box-shadow: 0 0 8px 0 #32949F;\n}\n.inputIcon[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\n    padding-left: 40px;\n}\n.inputIcon[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\n    padding-left: 40px;\n}\n.inputIcon[_ngcontent-%COMP%]{\n    position: relative;\n}\n.inputIcon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n    position: absolute;\n    left :6px;\n    top:33px;\n    padding: 9px, 8px;\n    color: #aaa;\n    transition: .3s;\n}\n.inputIcon[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:focus    + i[_ngcontent-%COMP%]{\n    color:#32949F;\n}\n.registerBox[_ngcontent-%COMP%]{\n    background-color: white;\n    width:50%;\n    height:40%;\n\n    margin-top: 42%;\n    margin-left:25%;\n    position: relative;\n    box-shadow: 0 15px 25px -10px rgba(0,0,0,.75);\n    transition: transform 300ms;\n}\n.registerBox[_ngcontent-%COMP%]:hover{\n    transform:scale(1.1)\n}\n.registerBox[_ngcontent-%COMP%]   .section[_ngcontent-%COMP%]{\n    padding-top: 0px;\n    margin:13%;\n    font-size:13px;\n    font-style: italic;\n}\n.registerBox[_ngcontent-%COMP%]   .specification[_ngcontent-%COMP%]{\n    padding-top: 35px;\n    padding-left:20px; \n    font-weight: bold;\n    text-decoration: underline;\n}\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:75%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\n.footer[_ngcontent-%COMP%]   address[_ngcontent-%COMP%]{\n    float: right;\n    font-size: 12px;\n    margin-top:-7%;\n    margin-right:30px;\n    text-align: left;\n}\n.mailus[_ngcontent-%COMP%]{\n  font-size: 12px;\n  margin-top:-2%;\n padding: 12px;\n  margin-left:63%;\n  text-align: left;\n}\n.vl[_ngcontent-%COMP%] {\n  border-left: 1px solid black;\n  height: 83px;\n  margin-right:10%;\n  margin-left:-6%;\n  position: absolute;\n  margin-top:0px;\n}\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRDQUE4RDtJQUM5RCxzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQixnQkFBZ0I7O0FBRXBCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7SUFDWixhQUFhO0lBQ2IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsNENBQTRDO0FBQ2hEO0FBQ0E7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtJQUNiLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIseUJBQXlCOztBQUU3QjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGNBQWM7QUFDbEI7QUFDQTtJQUNJLFlBQVk7SUFDWixVQUFVO0lBQ1YscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGNBQWM7O0FBRWxCO0FBQ0E7SUFDSSxjQUFjO0lBQ2Q7QUFDSjtBQUNBO0VBQ0UsWUFBWTtBQUNkO0FBRUE7SUFDSSxvQkFBb0I7SUFDcEIsNkJBQTZCO0FBQ2pDO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsU0FBUztJQUNULFFBQVE7SUFDUixpQkFBaUI7SUFDakIsV0FBVztJQUNYLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGFBQWE7QUFDakI7QUFFQTtJQUNJLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsVUFBVTs7SUFFVixlQUFlO0lBQ2YsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQiw2Q0FBNkM7SUFDN0MsMkJBQTJCO0FBQy9CO0FBQ0E7SUFDSTtBQUNKO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLGNBQWM7SUFDZCxrQkFBa0I7QUFDdEI7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLDBCQUEwQjtBQUM5QjtBQUVBOztJQUVJLGNBQWM7O0tBRWIsY0FBYztLQUNkLGlCQUFpQjtLQUNqQixhQUFhO0tBQ2IsU0FBUztLQUNULFVBQVU7SUFDWCxrQkFBa0I7S0FDakIsdUJBQXVCO1FBQ3BCLGtCQUFrQjtLQUNyQixrQkFBa0I7S0FDbEIsaUJBQWlCO0dBQ25CO0FBQ0E7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztDQUNmLGFBQWE7RUFDWixlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSw0QkFBNEI7RUFDNUIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7QUFDaEI7QUFDRztPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCO0FBQ0Y7S0FDSSxXQUFXO0tBQ1gsZUFBZTs7S0FFZixjQUFjO0NBQ2xCO0FBQ0E7S0FDSSxnQkFBZ0I7Q0FDcEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7QUFDQTtLQUNJLFdBQVc7S0FDWCxlQUFlO0tBQ2YsaUJBQWlCOztDQUVyQjtBQUNBO0tBQ0ksYUFBYTtLQUNiO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tYmcge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL2Jvb2suanBnKSBuby1yZXBlYXQgY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtby1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tcy1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgXG59XG4udGl0bGV7XG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICBtYXJnaW4tdG9wOjUlO1xufVxuLmdlbmVyYWwge1xuICAgIGhlaWdodDogODAwcHg7XG4gICAgd2lkdGg6IDQ5NXB4O1xuICAgIG1hcmdpbi10b3A6OSU7XG4gICAgbWFyZ2luLWxlZnQ6MTUlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAvKiBib3gtc2hhZG93OjAgMnB4IDEyMHB4IHJnYmEoMCwwLDAsLjc1KTsgKi9cbn1cbi5nZW5lcmFsIGxhYmVse1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xufVxuLmNvbnRhY3R7XG4gICAgaGVpZ2h0OiA4MDBweDtcbiAgICB3aWR0aDogNDk1cHg7XG4gICAgbWFyZ2luLXRvcDo5JTtcbiAgICBtYXJnaW4tbGVmdDo1MSU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM3Q0I3QzY7XG5cbn1cbi5mb3JtMXtcbiAgICBtYXJnaW4tbGVmdDogMTAlO1xuICAgIG1hcmdpbi10b3A6IDMlO1xufVxuLmdlbmVyYWwgaW5wdXQsc2VsZWN0e1xuICAgIGhlaWdodDogMzBweDtcbiAgICB3aWR0aDogNzUlO1xuICAgIGJvcmRlcjoycHggc29saWQgI2FhYTtcbiAgICBib3JkZXItcmFkaXVzOjRweDtcbiAgICBtYXJnaW46M3B4IDA7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBwYWRkaW5nOjVweDtcbiAgICBib3gtc2l6aW5nOmJvcmRlci1ib3g7XG4gICAgdHJhbnNpdGlvbjogLjNzO1xuICAgIGZvbnQtc2l6ZToxNnB4O1xuICAgXG59XG4uZ2VuZXJhbCAuZm9ybTEgcHtcbiAgICBmb250LXNpemU6MTJweDtcbiAgICBmb250LXN0eWxlOiBpdGFsaWNcbn1cbnNlbGVjdHtcbiAgaGVpZ2h0OiAzNHB4O1xufVxuXG4uZ2VuZXJhbCBpbnB1dDpmb2N1cyxzZWxlY3Q6Zm9jdXN7XG4gICAgYm9yZGVyLWNvbG9yOiMzMjk0OUY7XG4gICAgYm94LXNoYWRvdzogMCAwIDhweCAwICMzMjk0OUY7XG59XG4uaW5wdXRJY29uIGlucHV0e1xuICAgIHBhZGRpbmctbGVmdDogNDBweDtcbn1cbi5pbnB1dEljb24gc2VsZWN0e1xuICAgIHBhZGRpbmctbGVmdDogNDBweDtcbn1cbi5pbnB1dEljb257XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmlucHV0SWNvbiBpe1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0IDo2cHg7XG4gICAgdG9wOjMzcHg7XG4gICAgcGFkZGluZzogOXB4LCA4cHg7XG4gICAgY29sb3I6ICNhYWE7XG4gICAgdHJhbnNpdGlvbjogLjNzO1xufVxuLmlucHV0SWNvbiA6Zm9jdXMgKyBpe1xuICAgIGNvbG9yOiMzMjk0OUY7XG59XG5cbi5yZWdpc3RlckJveHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDo1MCU7XG4gICAgaGVpZ2h0OjQwJTtcblxuICAgIG1hcmdpbi10b3A6IDQyJTtcbiAgICBtYXJnaW4tbGVmdDoyNSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJveC1zaGFkb3c6IDAgMTVweCAyNXB4IC0xMHB4IHJnYmEoMCwwLDAsLjc1KTtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMzAwbXM7XG59XG4ucmVnaXN0ZXJCb3g6aG92ZXJ7XG4gICAgdHJhbnNmb3JtOnNjYWxlKDEuMSlcbn1cbi5yZWdpc3RlckJveCAuc2VjdGlvbntcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIG1hcmdpbjoxMyU7XG4gICAgZm9udC1zaXplOjEzcHg7XG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuXG4ucmVnaXN0ZXJCb3ggLnNwZWNpZmljYXRpb257XG4gICAgcGFkZGluZy10b3A6IDM1cHg7XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7IFxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG4uZm9vdGVyIHtcbiBcbiAgICBtYXJnaW4tbGVmdDo2JTtcbiAgICBcbiAgICAgbWFyZ2luLXRvcDo3NSU7XG4gICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICBib3R0b206IDA7XG4gICAgIHdpZHRoOiA4OSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGJvcmRlci1yYWRpdXM6NXB4O1xuICAgfVxuICAgLmZvb3RlciBhZGRyZXNze1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbWFyZ2luLXRvcDotNyU7XG4gICAgbWFyZ2luLXJpZ2h0OjMwcHg7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi5tYWlsdXN7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDotMiU7XG4gcGFkZGluZzogMTJweDtcbiAgbWFyZ2luLWxlZnQ6NjMlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLnZsIHtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCBibGFjaztcbiAgaGVpZ2h0OiA4M3B4O1xuICBtYXJnaW4tcmlnaHQ6MTAlO1xuICBtYXJnaW4tbGVmdDotNiU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDowcHg7XG59XG4gICAuZm9vdGVyLWljb257XG4gICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgbWFyZ2luLXRvcDoxNXB4O1xuICAgICAgIG1hcmdpbi1sZWZ0OjE1cHg7XG4gICB9XG4gLmxvZ28tZm9vdGVye1xuICAgICBmbG9hdDogbGVmdDtcbiAgICAgbWFyZ2luLXRvcDoyMXB4O1xuICAgICBcbiAgICAgZm9udC1zaXplOjIwcHg7XG4gfVxuIC5mYi1pY29ue1xuICAgICBtYXJnaW4tbGVmdDotMTAlO1xuIH1cbiAjc29jaWFsLWZiOmhvdmVyIHtcbiAgICAgY29sb3I6ICMzQjU5OTg7XG4gfVxuICNzb2NpYWwtdHc6aG92ZXIge1xuICAgICBjb2xvcjogIzQwOTlGRjtcbiB9XG4gI3NvY2lhbC1ncDpob3ZlciB7XG4gICAgIGNvbG9yOiAjZDM0ODM2O1xuIH1cbiAjc29jaWFsLWVtOmhvdmVyIHtcbiAgICAgY29sb3I6ICNmMzljMTI7XG4gfVxuIC5zb2NpYWwtaWNvbnN7XG4gICAgIGZsb2F0OiBsZWZ0O1xuICAgICBtYXJnaW4tdG9wOjIwcHg7XG4gICAgIG1hcmdpbi1sZWZ0OjE3MHB4O1xuICAgICBcbiB9XG4gLnNvY2lhbC1pY29ucyBhe1xuICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICBjb2xvcjpyZ2IoOTAsIDIwMSwgMjM4KVxuIH1cbiJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RegisterComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-register',
            templateUrl: './register.component.html',
            styleUrls: ['./register.component.css']
          }]
        }], function () {
          return [{
            type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/signin/signin.component.ts":
    /*!********************************************!*\
      !*** ./src/app/signin/signin.component.ts ***!
      \********************************************/

    /*! exports provided: SigninComponent */

    /***/
    function srcAppSigninSigninComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SigninComponent", function () {
        return SigninComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth.service.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      function SigninComponent_span_15_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " *email is required! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SigninComponent_span_15_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " *email is invalid! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SigninComponent_span_15_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SigninComponent_span_15_div_1_Template, 2, 0, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, SigninComponent_span_15_div_2_Template, 2, 0, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.email);
        }
      }

      function SigninComponent_span_20_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " *password is required! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SigninComponent_span_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SigninComponent_span_20_div_1_Template, 2, 0, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r3.errors.required);
        }
      }

      var SigninComponent = /*#__PURE__*/function () {
        function SigninComponent(_userService, router, _authService) {
          _classCallCheck(this, SigninComponent);

          this._userService = _userService;
          this.router = router;
          this._authService = _authService;
        }

        _createClass(SigninComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getUsers();
          }
        }, {
          key: "getUsers",
          value: function getUsers() {
            var _this2 = this;

            this._userService.getUsers().subscribe(function (users) {
              return _this2.allusers = users;
            }, function (err) {
              return console.log(err);
            });
          }
        }, {
          key: "signin",
          value: function signin(formValue) {
            console.log(formValue);

            for (var i = 0; i < this.allusers.length; i++) {
              if (this.allusers[i].email == formValue.username) {
                if (this.allusers[i].password != formValue.pwd) {
                  console.log("pwd not matched");
                  alert("Password doesn't match");
                  break;
                }

                console.log(this.allusers[i].email);
                var usr = this.allusers[i];
                localStorage.setItem("currentUser", JSON.stringify(usr));
                console.log(localStorage);
                break;
              }
            }

            if (!this._authService.isLoggedIn()) {
              alert("Please Try to login with valid credentials. If you're not registered then sign up");
            } else {
              // console.log(localStorage.getItem('currentUser'))
              this.router.navigate(['/']);
            }
          }
        }]);

        return SigninComponent;
      }();

      SigninComponent.ɵfac = function SigninComponent_Factory(t) {
        return new (t || SigninComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]));
      };

      SigninComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SigninComponent,
        selectors: [["app-signin"]],
        decls: 63,
        vars: 3,
        consts: [[1, "main-bg"], [1, "sub-main-w3"], [1, "bg-content-w3pvt"], [1, "top-content-style"], ["src", "../../assets/images/user.jpg", "alt", ""], [3, "ngSubmit"], ["loginForm", "ngForm"], [1, "legend", 2, "margin-top", "22px"], [1, "fa", "fa-hand-o-down"], [1, "input"], ["type", "email", "name", "username", "placeholder", "Email", "ngModel", "", "required", ""], ["usernameRef", "ngModel"], [1, "fa", "fa-envelope"], [4, "ngIf"], ["type", "password", "name", "pwd", "placeholder", "Password", "ngModel", "", "required", ""], ["pwdRef", "ngModel"], [1, "fa", "fa-unlock"], ["type", "submit", 1, "btn", "submit", 3, "disabled"], [2, "color", "white"], [1, "fas", "fa-sign-in-alt", "fa-1x"], [1, "bottom-text"], [1, "fas", "fa-hand-point-right"], ["routerLink", "/register", 1, "bottom-text-w3ls"], [1, "footer"], [1, "fas", "fa-book", "fa-3x", "footer-icon"], [1, "logo-footer"], [1, "social-icons"], ["href", "https://www.facebook.com/"], ["id", "social-fb", 1, "fab", "fa-facebook-square", "fa-3x", "social"], ["href", "https://twitter.com/"], ["id", "social-tw", 1, "fab", "fa-twitter-square", "fa-3x", "social"], ["href", "https://plus.google.com/"], ["id", "social-gp", 1, "fab", "fa-google-plus-square", "fa-3x", "social"], ["href", "mailto:bootsnipp@gmail.com"], ["id", "social-em", 1, "fa", "fa-envelope-square", "fa-3x", "social"], [1, "mailus"], [1, "vl"], ["class", "errorText", 4, "ngIf"], [1, "errorText"]],
        template: function SigninComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "h1");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "form", 5, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function SigninComponent_Template_form_ngSubmit_6_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](7);

              return ctx.signin(_r0.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Login Here");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 10, 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, SigninComponent_span_15_Template, 3, 2, "span", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 14, 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "span", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, SigninComponent_span_20_Template, 2, 1, "span", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Don't have an account");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "signup");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "BookZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "i", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "a", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "i", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "i", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "div", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Mail us at:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, " Example.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " Box 564, Disneyland");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, " Bangalore ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Visit us at");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, " BookZone Internet Private Limited,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, " Buildings Alyssa, Begonia &");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, " Clove Embassy Tech Village,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](7);

            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](13);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r3.invalid && (_r3.dirty || _r3.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r0.invalid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"]],
        styles: [".main-bg[_ngcontent-%COMP%] {\n    background: url('book.jpg') no-repeat center;\n    background-size: cover;\n    -webkit-background-size: cover;\n    -moz-background-size: cover;\n    -o-background-size: cover;\n    -ms-background-size: cover;\n    min-height: 100vh;\n    overflow: hidden;\n    \n    \n}\n\n\n\nh1[_ngcontent-%COMP%]{\n    font-size: 2.8vw;\n    color: #fff;\n    text-align: center;\n    padding: 1.5vw 1vw 2vw;\n    letter-spacing: 3px;\n    text-transform: uppercase;\n}\n\n\n\n\n\n.sub-main-w3[_ngcontent-%COMP%] {\n    margin: 5.5vw 2vw;\n   \n}\n\n.bg-content-w3pvt[_ngcontent-%COMP%] {\n    max-width: 400px;\n    margin: 0 auto;\n    background: #fff;\n    text-align: center;\n}\n\n.top-content-style[_ngcontent-%COMP%] {\n    padding: 2vw 4vw 4vw;\n    background: black;\n    height: 100px;\n    margin-top: 12%;\n\n}\n\n.top-content-style[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    border-radius: 50%;\n}\n\n.sub-main-w3[_ngcontent-%COMP%]   form[_ngcontent-%COMP%] {\n    background: #ffff;\n    padding: 2em;\n    box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);\n    margin: -2.5em 2.5em 2em;\n    border-radius: 4px;\n}\n\np.legend[_ngcontent-%COMP%] {\n    color: #4e4d4d;\n    font-size: 24px;\n    text-align: center;\n    margin-bottom: 1.2em;\n}\n\np.legend[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    color: #000;\n    margin-left: 10px;\n}\n\n.input[_ngcontent-%COMP%] {\n    position: relative;\n    margin: 20px auto;\n    width: 100%\n}\n\n.input[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    position: absolute;\n    display: block;\n    color: black;\n    left: 10px;\n    top: 12px;\n    font-size: 16px;\n}\n\n.input[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n    width: 100%;\n    padding: 13px 10px 13px 34px;\n    display: block;\n    border: none;\n    border: 1px solid black;\n    color: #000;\n    box-sizing: border-box;\n    font-size: 13px;\n    outline: none;\n    letter-spacing: 1px;\n    background: #fff;\n    box-shadow: 2px 5px 16px 2px rgba(16, 16, 16, 0.18);\n}\n\n.submit[_ngcontent-%COMP%] {\n    width: 45px;\n    height: 45px;\n    display: block;\n    margin: 2.5em auto 0;\n    background: black;\n    border-radius: 10px;\n    border: none;\n    cursor: pointer;\n    transition: 0.5s all;\n}\n\n.submit[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n    color: #fff;\n    font-size: 20px;\n}\n\n.submit[_ngcontent-%COMP%]:hover {\n    opacity: .8;\n    transition: 0.5s all;\n}\n\na.bottom-text-w3ls[_ngcontent-%COMP%] {\n    color: #757474;\n    font-size: 16px;\n    display: inline-block;\n    margin: 0em 0em 2em;\n    letter-spacing: 1px;\n}\n\np.bottom-text[_ngcontent-%COMP%]{\n    color: #757474;\n    font-size: 13px;\n    display: inline-block;\n    \n    letter-spacing: 1px;\n}\n\n\n\n.input[_ngcontent-%COMP%]   .errorText[_ngcontent-%COMP%]{\n    margin-left:0px;\n    margin-top: 33px;\n    font-size:13px;\n    float: left;\n    color: red\n}\n\n.footer[_ngcontent-%COMP%] {\n \n    margin-left:6%;\n    \n     margin-top:10%;\n     margin-bottom: 2%;\n     height: 100px;\n     bottom: 0;\n     width: 89%;\n    position: relative;\n     background-color: white;\n        position: relative;\n     text-align: center;\n     border-radius:5px;\n   }\n\n.footer[_ngcontent-%COMP%]   address[_ngcontent-%COMP%]{\n    float: right;\n    font-size: 12px;\n    margin-top:-7%;\n    margin-right:30px;\n    text-align: left;\n}\n\n.mailus[_ngcontent-%COMP%]{\n  font-size: 12px;\n  margin-top:-2%;\n padding: 12px;\n  margin-left:63%;\n  text-align: left;\n}\n\n.vl[_ngcontent-%COMP%] {\n  border-left: 1px solid black;\n  height: 83px;\n  margin-right:10%;\n  margin-left:-6%;\n  position: absolute;\n  margin-top:0px;\n}\n\n.footer-icon[_ngcontent-%COMP%]{\n       float: left;\n       margin-top:15px;\n       margin-left:15px;\n   }\n\n.logo-footer[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:21px;\n     \n     font-size:20px;\n }\n\n.fb-icon[_ngcontent-%COMP%]{\n     margin-left:-10%;\n }\n\n#social-fb[_ngcontent-%COMP%]:hover {\n     color: #3B5998;\n }\n\n#social-tw[_ngcontent-%COMP%]:hover {\n     color: #4099FF;\n }\n\n#social-gp[_ngcontent-%COMP%]:hover {\n     color: #d34836;\n }\n\n#social-em[_ngcontent-%COMP%]:hover {\n     color: #f39c12;\n }\n\n.social-icons[_ngcontent-%COMP%]{\n     float: left;\n     margin-top:20px;\n     margin-left:170px;\n     \n }\n\n.social-icons[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\n     padding: 10px;\n     color:rgb(90, 201, 238)\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbmluL3NpZ25pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksNENBQThEO0lBQzlELHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsMkJBQTJCO0lBQzNCLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsaUJBQWlCO0lBQ2pCLGdCQUFnQjs7O0FBR3BCOztBQUVBLFVBQVU7O0FBQ1Y7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLHlCQUF5QjtBQUM3Qjs7QUFFQSxZQUFZOztBQUVaLFlBQVk7O0FBQ1o7SUFDSSxpQkFBaUI7O0FBRXJCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsZUFBZTs7QUFFbkI7O0FBRUE7SUFLSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsWUFBWTtJQUdaLG1EQUFtRDtJQUNuRCx3QkFBd0I7SUFLeEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksV0FBVztJQUNYLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakI7QUFDSjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsWUFBWTtJQUNaLFVBQVU7SUFDVixTQUFTO0lBQ1QsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCw0QkFBNEI7SUFDNUIsY0FBYztJQUNkLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLHNCQUFzQjtJQUN0QixlQUFlO0lBQ2YsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFHaEIsbURBQW1EO0FBQ3ZEOztBQUVBO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWixjQUFjO0lBQ2Qsb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUtqQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGVBQWU7SUFLZixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFdBQVc7SUFLWCxvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsbUJBQW1CO0FBQ3ZCOztBQUNBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixxQkFBcUI7O0lBRXJCLG1CQUFtQjtBQUN2Qjs7QUFFQSxjQUFjOztBQUNkO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsV0FBVztJQUNYO0FBQ0o7O0FBRUE7O0lBRUksY0FBYzs7S0FFYixjQUFjO0tBQ2QsaUJBQWlCO0tBQ2pCLGFBQWE7S0FDYixTQUFTO0tBQ1QsVUFBVTtJQUNYLGtCQUFrQjtLQUNqQix1QkFBdUI7UUFDcEIsa0JBQWtCO0tBQ3JCLGtCQUFrQjtLQUNsQixpQkFBaUI7R0FDbkI7O0FBQ0E7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztBQUNBO0VBQ0UsZUFBZTtFQUNmLGNBQWM7Q0FDZixhQUFhO0VBQ1osZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjs7QUFDQTtFQUNFLDRCQUE0QjtFQUM1QixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsY0FBYztBQUNoQjs7QUFDRztPQUNJLFdBQVc7T0FDWCxlQUFlO09BQ2YsZ0JBQWdCO0dBQ3BCOztBQUNGO0tBQ0ksV0FBVztLQUNYLGVBQWU7O0tBRWYsY0FBYztDQUNsQjs7QUFDQTtLQUNJLGdCQUFnQjtDQUNwQjs7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7O0FBQ0E7S0FDSSxjQUFjO0NBQ2xCOztBQUNBO0tBQ0ksY0FBYztDQUNsQjs7QUFDQTtLQUNJLGNBQWM7Q0FDbEI7O0FBQ0E7S0FDSSxXQUFXO0tBQ1gsZUFBZTtLQUNmLGlCQUFpQjs7Q0FFckI7O0FBQ0E7S0FDSSxhQUFhO0tBQ2I7Q0FDSiIsImZpbGUiOiJzcmMvYXBwL3NpZ25pbi9zaWduaW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWJnIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9ib29rLmpwZykgbm8tcmVwZWF0IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW8tYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbXMtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIFxuICAgIFxufVxuXG4vKiB0aXRsZSAqL1xuaDF7XG4gICAgZm9udC1zaXplOiAyLjh2dztcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMS41dncgMXZ3IDJ2dztcbiAgICBsZXR0ZXItc3BhY2luZzogM3B4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi8qIC8vdGl0bGUgKi9cblxuLyogY29udGVudCAqL1xuLnN1Yi1tYWluLXczIHtcbiAgICBtYXJnaW46IDUuNXZ3IDJ2dztcbiAgIFxufVxuXG4uYmctY29udGVudC13M3B2dCB7XG4gICAgbWF4LXdpZHRoOiA0MDBweDtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnRvcC1jb250ZW50LXN0eWxlIHtcbiAgICBwYWRkaW5nOiAydncgNHZ3IDR2dztcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIG1hcmdpbi10b3A6IDEyJTtcblxufVxuXG4udG9wLWNvbnRlbnQtc3R5bGUgaW1nIHtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAtby1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5zdWItbWFpbi13MyBmb3JtIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZjtcbiAgICBwYWRkaW5nOiAyZW07XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAycHggNXB4IDE2cHggMnB4IHJnYmEoMTYsIDE2LCAxNiwgMC4xOCk7XG4gICAgLW1vei1ib3gtc2hhZG93OiAycHggNXB4IDE2cHggMnB4IHJnYmEoMTYsIDE2LCAxNiwgMC4xOCk7XG4gICAgYm94LXNoYWRvdzogMnB4IDVweCAxNnB4IDJweCByZ2JhKDE2LCAxNiwgMTYsIDAuMTgpO1xuICAgIG1hcmdpbjogLTIuNWVtIDIuNWVtIDJlbTtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAtby1ib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbnAubGVnZW5kIHtcbiAgICBjb2xvcjogIzRlNGQ0ZDtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDEuMmVtO1xufVxuXG5wLmxlZ2VuZCBzcGFuIHtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLmlucHV0IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XG4gICAgd2lkdGg6IDEwMCVcbn1cblxuLmlucHV0IHNwYW4ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgbGVmdDogMTBweDtcbiAgICB0b3A6IDEycHg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4uaW5wdXQgaW5wdXQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDEzcHggMTBweCAxM3B4IDM0cHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMnB4IDVweCAxNnB4IDJweCByZ2JhKDE2LCAxNiwgMTYsIDAuMTgpO1xuICAgIC1tb3otYm94LXNoYWRvdzogMnB4IDVweCAxNnB4IDJweCByZ2JhKDE2LCAxNiwgMTYsIDAuMTgpO1xuICAgIGJveC1zaGFkb3c6IDJweCA1cHggMTZweCAycHggcmdiYSgxNiwgMTYsIDE2LCAwLjE4KTtcbn1cblxuLnN1Ym1pdCB7XG4gICAgd2lkdGg6IDQ1cHg7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogMi41ZW0gYXV0byAwO1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAtby1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIC1tcy1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIC1tb3otYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjVzIGFsbDtcbiAgICAtby10cmFuc2l0aW9uOiAwLjVzIGFsbDtcbiAgICAtbW96LXRyYW5zaXRpb246IDAuNXMgYWxsO1xuICAgIC1tcy10cmFuc2l0aW9uOiAwLjVzIGFsbDtcbiAgICB0cmFuc2l0aW9uOiAwLjVzIGFsbDtcbn1cblxuLnN1Ym1pdCBzcGFuIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5zdWJtaXQ6aG92ZXIge1xuICAgIG9wYWNpdHk6IC44O1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogMC41cyBhbGw7XG4gICAgLW8tdHJhbnNpdGlvbjogMC41cyBhbGw7XG4gICAgLW1vei10cmFuc2l0aW9uOiAwLjVzIGFsbDtcbiAgICAtbXMtdHJhbnNpdGlvbjogMC41cyBhbGw7XG4gICAgdHJhbnNpdGlvbjogMC41cyBhbGw7XG59XG5cbmEuYm90dG9tLXRleHQtdzNscyB7XG4gICAgY29sb3I6ICM3NTc0NzQ7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDBlbSAwZW0gMmVtO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5wLmJvdHRvbS10ZXh0e1xuICAgIGNvbG9yOiAjNzU3NDc0O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgXG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cblxuLyogLy9jb250ZW50ICovXG4uaW5wdXQgLmVycm9yVGV4dHtcbiAgICBtYXJnaW4tbGVmdDowcHg7XG4gICAgbWFyZ2luLXRvcDogMzNweDtcbiAgICBmb250LXNpemU6MTNweDtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBjb2xvcjogcmVkXG59XG5cbi5mb290ZXIge1xuIFxuICAgIG1hcmdpbi1sZWZ0OjYlO1xuICAgIFxuICAgICBtYXJnaW4tdG9wOjEwJTtcbiAgICAgbWFyZ2luLWJvdHRvbTogMiU7XG4gICAgIGhlaWdodDogMTAwcHg7XG4gICAgIGJvdHRvbTogMDtcbiAgICAgd2lkdGg6IDg5JTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgYm9yZGVyLXJhZGl1czo1cHg7XG4gICB9XG4gICAuZm9vdGVyIGFkZHJlc3N7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW4tdG9wOi03JTtcbiAgICBtYXJnaW4tcmlnaHQ6MzBweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLm1haWx1c3tcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tdG9wOi0yJTtcbiBwYWRkaW5nOiAxMnB4O1xuICBtYXJnaW4tbGVmdDo2MyU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4udmwge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkIGJsYWNrO1xuICBoZWlnaHQ6IDgzcHg7XG4gIG1hcmdpbi1yaWdodDoxMCU7XG4gIG1hcmdpbi1sZWZ0Oi02JTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOjBweDtcbn1cbiAgIC5mb290ZXItaWNvbntcbiAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICBtYXJnaW4tdG9wOjE1cHg7XG4gICAgICAgbWFyZ2luLWxlZnQ6MTVweDtcbiAgIH1cbiAubG9nby1mb290ZXJ7XG4gICAgIGZsb2F0OiBsZWZ0O1xuICAgICBtYXJnaW4tdG9wOjIxcHg7XG4gICAgIFxuICAgICBmb250LXNpemU6MjBweDtcbiB9XG4gLmZiLWljb257XG4gICAgIG1hcmdpbi1sZWZ0Oi0xMCU7XG4gfVxuICNzb2NpYWwtZmI6aG92ZXIge1xuICAgICBjb2xvcjogIzNCNTk5ODtcbiB9XG4gI3NvY2lhbC10dzpob3ZlciB7XG4gICAgIGNvbG9yOiAjNDA5OUZGO1xuIH1cbiAjc29jaWFsLWdwOmhvdmVyIHtcbiAgICAgY29sb3I6ICNkMzQ4MzY7XG4gfVxuICNzb2NpYWwtZW06aG92ZXIge1xuICAgICBjb2xvcjogI2YzOWMxMjtcbiB9XG4gLnNvY2lhbC1pY29uc3tcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgbWFyZ2luLWxlZnQ6MTcwcHg7XG4gICAgIFxuIH1cbiAuc29jaWFsLWljb25zIGF7XG4gICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgIGNvbG9yOnJnYig5MCwgMjAxLCAyMzgpXG4gfVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SigninComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-signin',
            templateUrl: './signin.component.html',
            styleUrls: ['./signin.component.css']
          }]
        }], function () {
          return [{
            type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
          }, {
            type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/users/user.service.ts":
    /*!***************************************!*\
      !*** ./src/app/users/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcAppUsersUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var UserService = /*#__PURE__*/function () {
        function UserService(_http) {
          _classCallCheck(this, UserService);

          this._http = _http;
          this._userUrl = "http://13.92.132.47:3000/users";
          this.count = 3;
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
        }

        _createClass(UserService, [{
          key: "getUsers",
          value: function getUsers() {
            return this._http.get(this._userUrl);
          }
        }, {
          key: "addUser",
          value: function addUser(newUser) {
            console.log(newUser);
            return this._http.post(this._userUrl, newUser, this.httpOptions);
          }
        }]);

        return UserService;
      }();

      UserService.ɵfac = function UserService_Factory(t) {
        return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: UserService,
        factory: UserService.ɵfac
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/environments/environment.ts":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "./src/main.ts":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function srcMainTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "./src/app/app.module.ts");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /home/cicd/CAP/capstone-devops-Dev/src/main.ts */
      "./src/main.ts");
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map